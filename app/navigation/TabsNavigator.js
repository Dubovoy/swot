// @flow

import * as React from 'react'
import { createBottomTabNavigator, createAppContainer } from 'react-navigation'
import Icon from 'react-native-vector-icons/Feather'

import SwotListNavigator from './SwotListNavigator'
import QuestionPacksNavigator from './ QuestionPacksNavigator'

// Styles
import palette from '../styles/palette'

// Locales
import { localizedString } from '../locales/localizedString'

const TabNames = {
    SwotList: 'SwotList',
    QuestionPacks: 'QuestionPacks',
}

export const Tabs = {
    [TabNames.SwotList]: {
        routeName: TabNames.SwotList,
        title: localizedString('tabs.swots'),
        iconName: 'list',
    },
    [TabNames.QuestionPacks]: {
        routeName: TabNames.QuestionPacks,
        title: localizedString('tabs.questionPacks'),
        iconName: 'package',
    },
}

const TabNavigator = createBottomTabNavigator(
    {
        [TabNames.SwotList]: SwotListNavigator,
        [TabNames.QuestionPacks]: QuestionPacksNavigator,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            title: Tabs[navigation.state.routeName].title,
            tabBarIcon: () => {
                const { routeName } = navigation.state
                const { iconName } = Tabs[routeName]
                return (
                    <Icon
                        name={iconName}
                        size={25}
                        color={palette.navbar}
                    />
                )
            },
        }),
        tabBarOptions: {
            activeTintColor: palette.navbar,
            inactiveTintColor: palette.mainText,
        },
    }
)

export default createAppContainer(TabNavigator)
