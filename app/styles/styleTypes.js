// @flow

import { type ____ViewStyle_Internal as ViewStyleT } from 'react-native/Libraries/StyleSheet/StyleSheetTypes'

export type ViewStyle = ViewStyleT
