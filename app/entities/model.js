// @flow strict

export type SectionName =
    | 'Strengthes'
    | 'Weaknesses'
    | 'Opportunities'
    | 'Threats'
    | 'NOT_DEFINED'

export type SwotItem = {|
    +id: number,
    +swotId: number,
    +section: SectionName,
    +text: string,
    +questionId: number,
|}

export type Swot = {|
    +id: number,
    +title: string,
|}

export type Question = {|
    +id: number,
    +packId: number,
    +section: SectionName,
    +text: string,
|}
