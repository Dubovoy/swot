// @flow

// Library tools
import * as React from 'react'
import { connect } from 'react-redux'
import Swipeout from 'react-native-swipeout'

// Library components
// eslint-disable-next-line react-native/split-platform-components
import { AlertIOS, FlatList, SafeAreaView, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

// App components
import withTouchHandlers from '../../components/HOCs/withTouchHandlers'
import FlatListFooterStub from '../../components/FlatListFooterStub'

// Styles
import layouts from '../../styles/layouts'
import palette from '../../styles/palette'

// Locales
import { localizedString } from '../../locales/localizedString'

// Business logic
import { getSwotList } from './swotList'
import { addNewSwot, deleteSwot } from './swotListAsyncActions'
import { prepareSwot } from '../currentSwot/swotAsyncActions'

// Entities
import { type Swot } from '../../entities/model'
import { type FlatListRenderItemInfo } from '../../entities/flowTypes'
import { type RootState } from '../rootReducer'

// Navigation
import {
    type Navigation,
    makeNavigationOptions,
} from '../../navigation/navigationEntities'

type InitalNavData = {|
    +onAddSwot: (text: string) => void,
|}

type StateMapProps = {|
    +swots: Array<Swot>,
|}

type DispatchMapProps = {|
    +onAddSwot: (text: string) => void,
    +onOpenSwot: (id: number, title: string) => void,
    +onDeleteSwot: (id: number) => void,
|}

type NavigationProps = {|
    +navigation: Navigation<InitalNavData>,
|}

type Props = StateMapProps & DispatchMapProps & NavigationProps

class SwotListScreen extends React.Component<Props> {
    static navigationOptions = makeNavigationOptions<InitalNavData>(params => {
        const onAddSwot = params ? params.onAddSwot : null

        const AddButtonShape = () => (
            <View style={layouts.listSectionHeader.rightButton}>
                <Icon
                    name="plus-circle"
                    size={25}
                    color={palette.contrastLightText}
                />
            </View>
        )
        const AddButton = withTouchHandlers({
            senderKey: 0,
            onPress: () => {
                AlertIOS.prompt(
                    localizedString('components.addSwotButton.dialog.title'),
                    null,
                    text => (onAddSwot && onAddSwot(text)) || undefined
                )
            },
        })(AddButtonShape)

        return {
            title: localizedString('swotListScreen.title'),
            headerRight: <AddButton />,
        }
    })

    componentDidMount() {
        const { navigation, onAddSwot } = this.props
        navigation.setParams({ onAddSwot })
    }

    renderItem = (info: FlatListRenderItemInfo<Swot>): React.Element<any> => {
        const { item } = info
        const NameViewShape = () => (
            <View style={layouts.listItem.container}>
                <Text style={layouts.listItem.title}>{`${item.title}`}</Text>
            </View>
        )
        const { onOpenSwot, onDeleteSwot } = this.props
        const openSwot = () => {
            onOpenSwot(item.id, item.title)
        }
        const NameView = withTouchHandlers(
            { senderKey: item.id, onPress: openSwot },
            { senderKey: item.id, onLongPress: onDeleteSwot }
        )(NameViewShape)

        const swipeoutBtns = [
            {
                text: localizedString('components.swipeButton.title'),
                backgroundColor: palette.deleteRed,
                onPress: () => {
                    onDeleteSwot(item.id)
                },
            },
        ]

        return (
            <Swipeout right={swipeoutBtns}>
                <NameView />
            </Swipeout>
        )
    }

    separator = (): React.Element<any> => (
        <View style={layouts.listItem.separator} />
    )

    keyExtractor = (item: Swot): string => String(item.id)

    render(): React.Element<any> {
        const { swots } = this.props
        return (
            <SafeAreaView style={layouts.containers.allAvailableSpace}>
                <FlatList
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.separator}
                    data={swots}
                    keyExtractor={this.keyExtractor}
                    ListFooterComponent={<FlatListFooterStub height={50} />}
                />
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state: RootState): StateMapProps => ({
    swots: getSwotList(state),
})

const mapDispatchToProps = (
    dispatch,
    ownProps: NavigationProps
): DispatchMapProps => ({
    onAddSwot: title => dispatch(addNewSwot(title)),
    onDeleteSwot: (id: number) => dispatch(deleteSwot(id)),
    onOpenSwot: (id: number, title: string) => {
        dispatch(prepareSwot(id)).then(() => {
            // eslint-disable-next-line flowtype-errors/show-errors
            ownProps.navigation.navigate('SwotScreen', { title })
        })
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SwotListScreen)
