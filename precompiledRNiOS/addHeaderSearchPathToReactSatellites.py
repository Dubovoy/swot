import os
import sys
from os import walk
from addSearchPathHeaderToXCodeProj import addSearchPathHeaderToXCodeProj

headerSearchPath = sys.argv[1]

reactSatelliesPath ="../node_modules"
for (dirpath, dirnames, filenames) in walk(reactSatelliesPath):
    if not dirpath.endswith('React.xcodeproj'):
        if 'project.pbxproj' in filenames:
            print dirpath
            addSearchPathHeaderToXCodeProj(dirpath, headerSearchPath)
