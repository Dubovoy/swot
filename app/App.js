// @flow

import * as React from 'react'
import { YellowBox } from 'react-native'

// redux
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './store'
import { initReduxStore } from './features/initReduxStore'

// root react component
import SceneSelector from './navigation/SceneSelector'

// Utils
import { reportEvent } from './services/metrica'

reportEvent({ app: 'appStarted' })

// fixes https://github.com/facebook/react-native/issues/20841
YellowBox.ignoreWarnings(['Require cycle:'])

initReduxStore()

const App = () => (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <SceneSelector />
        </PersistGate>
    </Provider>
)
export default App
