#import "RCTSharePdf.h"
#import <UIKit/UIKit.h>

@implementation RCTSharePdf {

}

RCT_EXPORT_MODULE();

UIDocumentInteractionController* controller; // TODO: should release after using

RCT_EXPORT_METHOD(share:(NSString *)filePath)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    controller = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
    UIView *rootView = [UIApplication sharedApplication].delegate.window.rootViewController.view;
    [controller presentOptionsMenuFromRect:CGRectZero inView:rootView animated:YES];
  });
}

@end
