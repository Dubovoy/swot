// @flow

import * as React from 'react'
import {
    TouchableOpacity,
    TouchableNativeFeedback,
    Platform,
} from 'react-native'

const selectableBackground = TouchableNativeFeedback.SelectableBackground()

const TouchableComponent = props => {
    const { activeOpacity, onPress, onLongPress, children } = props
    if (Platform.OS === 'ios') {
        return (
            <TouchableOpacity
                activeOpacity={activeOpacity}
                onPress={onPress}
                onLongPress={onLongPress}
            >
                {children}
            </TouchableOpacity>
        )
    }
    if (Platform.OS === 'android') {
        return (
            <TouchableNativeFeedback
                activeOpacity={activeOpacity}
                onPress={onPress}
                onLongPress={onLongPress}
                background={selectableBackground}
            >
                {children}
            </TouchableNativeFeedback>
        )
    }
    return null
}

function getDisplayName(WrappedComponent): string {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component'
}

export default function withTouchHandlers<PressKey, LongPressKey>(
    pressHandler?: ?{
        senderKey: PressKey,
        onPress: (senderId: PressKey) => void,
    },
    longPressHandler?: ?{
        senderKey: LongPressKey,
        onLongPress: (senderId: LongPressKey) => void,
    },
    activeOpacity?: number
) {
    function handlePress() {
        if (!pressHandler) return
        const { senderKey, onPress } = pressHandler
        onPress && onPress instanceof Function && onPress(senderKey)
    }
    function handleLongPress() {
        if (!longPressHandler) return
        const { senderKey, onLongPress } = longPressHandler
        onLongPress && onLongPress instanceof Function && onLongPress(senderKey)
    }

    return (WrappedComponent: React.ComponentType<any>) => {
        const WithTouchHandlers = (passThroughProps: Object) => (
            <TouchableComponent
                activeOpacity={activeOpacity}
                onPress={handlePress}
                onLongPress={handleLongPress}
            >
                <WrappedComponent {...passThroughProps} />
            </TouchableComponent>
        )
        const displayName = getDisplayName(WrappedComponent)
        WithTouchHandlers.displayName = `WithTouchHandlers(${displayName})`
        return WithTouchHandlers
    }
}

export function withTouchHandlers3<SenderKey, SenderKey1>(
    pressHandler: {
        senderKey: SenderKey,
        onPress: (senderId: SenderKey) => void,
    },
    longPressHandler: {
        senderKey: SenderKey1,
        onLongPress: (senderId: SenderKey1) => void,
    },
    activeOpacity?: number
) {
    function handlePress() {
        const { senderKey, onPress } = pressHandler
        onPress && onPress instanceof Function && onPress(senderKey)
    }
    function handleLongPress() {
        const { senderKey, onLongPress } = longPressHandler
        onLongPress && onLongPress instanceof Function && onLongPress(senderKey)
    }

    return (WrappedComponent: React.ComponentType<any>) => {
        const WithTouchHandlers = (passThroughProps: Object) => (
            <TouchableComponent
                activeOpacity={activeOpacity}
                onPress={handlePress}
                onLongPress={handleLongPress}
            >
                <WrappedComponent {...passThroughProps} />
            </TouchableComponent>
        )
        const displayName = getDisplayName(WrappedComponent)
        WithTouchHandlers.displayName = `WithTouchHandlers(${displayName})`
        return WithTouchHandlers
    }
}
