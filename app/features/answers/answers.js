// @flow

import { createSelector } from 'reselect'

import { type SectionName } from '../../entities/model'

import {
    CarouselCard,
    QuestionCard,
    MoreQuestionsCard,
} from '../../entities/viewModel'

import { getIsLargestPurchased } from '../purchases/purchase'

import { type RootState } from '../rootReducer'

// Types
export type AnswersState = {|
    +swotId: number,
    +section: SectionName,
    +firstUnansweredCardIndex: number,
    +cards: Array<QuestionCard>,
    +answerChanges: { [questionId: number]: string },
|}

export type setSwotInfoAction = {|
    +type: 'SET_SWOT_INFO',
    +payload: {|
        +swotId: number,
        +section: SectionName,
        +firstUnansweredCardIndex: number,
    |},
|}

export type SetCardsAction = {|
    +type: 'SET_CARDS',
    +payload: {| +cards: Array<QuestionCard> |},
|}

export type updateAnswerChangesAction = {|
    +type: 'UPDATE_CHANGES',
    +payload: {| +questionId: number, answerText: string |},
|}

const NO_SWOT_ID = -1
const NO_SWOT_ITEM_ID = -1
export const NO_QUESTION_ID = -1

// Reducer
const defaultState: AnswersState = {
    swotId: NO_SWOT_ID,
    section: 'NOT_DEFINED',
    firstUnansweredCardIndex: NO_QUESTION_ID,
    cards: [],
    answerChanges: {},
}

export default function reducer(
    state: $Exact<AnswersState> = defaultState,
    action: setSwotInfoAction | SetCardsAction | updateAnswerChangesAction
): $Exact<AnswersState> {
    switch (action.type) {
        case 'SET_SWOT_INFO': {
            const { swotId, section, firstUnansweredCardIndex } = action.payload
            const answerChanges = {}
            return {
                ...state,
                swotId,
                section,
                firstUnansweredCardIndex,
                answerChanges,
            }
        }
        case 'SET_CARDS': {
            const { cards } = action.payload
            return { ...state, cards }
        }
        case 'UPDATE_CHANGES': {
            const { questionId, answerText } = action.payload
            const answerChanges = { ...state.answerChanges }
            answerChanges[questionId] = answerText
            return { ...state, answerChanges }
        }
        default: {
            return state
        }
    }
}

// Action creators

export const setSwotInfo = (
    swotId: number,
    section: SectionName,
    firstUnansweredCardIndex: number
): setSwotInfoAction => {
    const type = 'SET_SWOT_INFO'
    return { type, payload: { swotId, section, firstUnansweredCardIndex } }
}

export const setCards = (cards: Array<QuestionCard>): SetCardsAction => {
    const type = 'SET_CARDS'
    return { type, payload: { cards } }
}

export const updateAnswerChanges = (
    questionId: number,
    answerText: string
): updateAnswerChangesAction => {
    const type = 'UPDATE_CHANGES'
    return { type, payload: { questionId, answerText } }
}

// Selectors
export const getSectionName = (state: RootState): SectionName =>
    state.answers.section

export const getSwotId = (state: RootState): number => state.answers.swotId

export const getCards = createSelector(
    (state: RootState): Array<QuestionCard> => state.answers.cards,
    (state: RootState): boolean => getIsLargestPurchased(state),
    (
        questionCards: Array<QuestionCard>,
        isLargestPurchased: boolean
    ): Array<CarouselCard> =>
        isLargestPurchased
            ? [...questionCards]
            : [...questionCards, new MoreQuestionsCard()]
)

export const getFirstCardIndex = (state: RootState): number => {
    const { firstUnansweredCardIndex } = state.answers
    if (firstUnansweredCardIndex >= 0) {
        return firstUnansweredCardIndex
    }
    const cards = getCards(state)
    return cards.findIndex(card => Boolean(card instanceof MoreQuestionsCard))
}

export const getEditedAnswer = (
    state: RootState,
    questionId: number
): ?string => {
    const { answerChanges } = state.answers
    return answerChanges[questionId]
}

type AnswerChanges = {
    brandNewChanges: Array<{
        swotId: number,
        section: SectionName,
        text: string,
        questionId: number,
    }>,
    editedChanges: Array<{
        swotItemId: number,
        text: string,
    }>,
}

export const getAnswerChanges = createSelector(
    state => state.answers,
    (answersState: AnswersState): AnswerChanges => {
        const { swotId, section, cards, answerChanges } = answersState
        const changeKeys = Object.keys(answerChanges).map(Number)
        const editedChangeKeys = changeKeys.filter(questionId => {
            const questionCard = cards.find(
                card => card.question.id === questionId
            )
            return questionCard && questionCard.swotItem
        })
        const brandNewChangeKeys = changeKeys.filter(
            questionId => !editedChangeKeys.includes(questionId)
        )

        const brandNewChanges = brandNewChangeKeys.map(questionId => ({
            swotId,
            section,
            text: answerChanges[questionId],
            questionId,
        }))
        const editedChanges = editedChangeKeys.map(questionId => {
            const editedCard = cards.find(
                card => card.question.id === questionId
            )
            const swotItemId =
                editedCard && editedCard.swotItem && editedCard.swotItem.id
            return {
                swotItemId: swotItemId || NO_SWOT_ITEM_ID,
                text: answerChanges[questionId],
            }
        })

        return { brandNewChanges, editedChanges }
    }
)
