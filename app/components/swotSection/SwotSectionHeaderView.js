// @flow

// Library tools
import * as React from 'react'

// Library components
import { Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

// Entities
import { type SectionName } from '../../entities/model'
import { type SwotSection } from './SwotSection'
import { type SectionListRenderHeaderInfo } from '../../entities/flowTypes'

// App components
import withTouchHandlers from '../HOCs/withTouchHandlers'

// Styles
import layouts from '../../styles/layouts'
import palette from '../../styles/palette'

// Utils
import { unboundedMemoizedSelectorCreator } from '../../utils/memoize'

const getHeaderColor = (sectionName: SectionName) => {
    switch (sectionName) {
        case 'Strengthes':
            return palette.headerBackColorS
        case 'Weaknesses':
            return palette.headerBackColorW
        case 'Opportunities':
            return palette.headerBackColorO
        case 'Threats':
            return palette.headerBackColorT
        default:
            return palette.border
    }
}

const getHeaderTextColor = (sectionName: SectionName) => {
    switch (sectionName) {
        case 'Strengthes':
            return palette.contrastLightText
        default:
            return palette.mainText
    }
}
export function makeSwotSectionHeaderView(
    addSwotItem: (sectionName: SectionName) => void,
    expandOrCollapseSection: ({
        sectionName: SectionName,
        isMinimized: boolean,
    }) => void
) {
    // memoize to prevent blinking while list updating
    return unboundedMemoizedSelectorCreator(
        (swotSection: SectionListRenderHeaderInfo<SwotSection>) =>
            swotSection.section.name,
        (swotSection: SectionListRenderHeaderInfo<SwotSection>) =>
            swotSection.section.isMinimized,
        (swotSection: SectionListRenderHeaderInfo<SwotSection>) =>
            swotSection.section.isEmpty,
        (
            sectionName: SectionName,
            isMinimized: boolean,
            isEmpty: boolean
        ): React.Element<any> => {
            const iconName = isMinimized ? 'chevron-down' : 'chevron-up'
            const CollapseButtonShape = () => (
                <View style={layouts.listSectionHeader.leftButton}>
                    <Icon
                        name={iconName}
                        size={25}
                        color={palette.navbar}
                    />
                </View>
            )
            const ButtonStub = () => (
                <View style={layouts.listSectionHeader.leftButton} />
            )
            const pressHaddler = {
                senderKey: { sectionName, isMinimized: !isMinimized },
                onPress: expandOrCollapseSection,
            }
            const CollapseButton = isEmpty
                ? ButtonStub
                : withTouchHandlers(pressHaddler)(CollapseButtonShape)
            const AddButtonShape = () => (
                <View style={layouts.listSectionHeader.rightButton}>
                    <Icon
                        name="plus-circle"
                        size={25}
                        color={palette.navbar}
                    />
                </View>
            )
            const AddButton = withTouchHandlers({
                senderKey: sectionName,
                onPress: addSwotItem,
            })(AddButtonShape)
            const headerStyle = {
                ...layouts.listSectionHeader.container,
                backgroundColor: getHeaderColor(sectionName),
            }
            const headerTextStyle = {
                ...layouts.listSectionHeader.text,
                color: getHeaderTextColor(sectionName),
            }
            const SectionHeaderView = () => (
                <View style={headerStyle}>
                    <CollapseButton />
                    <Text style={headerTextStyle}>{`${sectionName}`}</Text>
                    <AddButton />
                </View>
            )
            return <SectionHeaderView />
        }
    )
}
