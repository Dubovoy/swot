// @flow

// Library tools
import * as React from 'react'
import { connect } from 'react-redux'
import { StatusBar } from 'react-native'

// App components
import OnboardingScreen from '../features/onboarding/OnboardingScreen'
import TabsNavigator from './TabsNavigator'

// Business logic
import { getWasShown } from '../features/onboarding/onboarding'

// Entities
import { type RootState } from '../features/rootReducer'

type StateMapProps = {|
    +wasShown: boolean,
|}

type DispatchMapProps = {}

type Props = StateMapProps & DispatchMapProps

const SceneSelector = ({ wasShown }: Props) => {
    const ResultScreen = wasShown ? TabsNavigator : OnboardingScreen
    StatusBar.setBarStyle(wasShown ? 'light-content' : 'dark-content', true)
    return <ResultScreen />
}

const mapStateToProps = (state: RootState): StateMapProps => ({
    wasShown: getWasShown(state),
})

const mapDispatchToProps = (): DispatchMapProps => ({})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SceneSelector)
