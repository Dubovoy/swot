// @flow

import { type Swot } from '../../entities/model'
import { type RootState } from '../rootReducer'

// Types
export type SwotListState = {|
    +swotList: Array<Swot>,
|}

export type UpdateActionA = {|
    +type: 'UPDATE_SWOT_LIST',
    +payload: {| list: Array<Swot> |},
|}

export type UpdateActionB = {|
    +type: 'UPDATE_SWOT_LIST2',
    +payload: {| list2: Array<Swot> |},
|}

// Reducer
const defaultState: SwotListState = {
    swotList: [],
}

export default function reducer(
    state: $Exact<SwotListState> = defaultState,
    action: UpdateActionA | UpdateActionB
): $Exact<SwotListState> {
    switch (action.type) {
        case 'UPDATE_SWOT_LIST': {
            const { list } = action.payload
            return { ...state, swotList: list }
        }
        case 'UPDATE_SWOT_LIST2': {
            const { list2 } = action.payload
            return { ...state, swotList: list2 }
        }
        default: {
            return state
        }
    }
}

// Action creators
export const updateSwotList = (list: Array<Swot>): UpdateActionA => {
    const type = 'UPDATE_SWOT_LIST'
    return { type, payload: { list } }
}

// Selectors
export const getSwotList = (state: RootState) => state.swotList.swotList
