// @flow
import { NativeModules } from 'react-native'

const { AppMetrica } = NativeModules

export function reportEvent(params: {}) {
    const eventType = __DEV__ ? 'debug' : 'production'
    AppMetrica.reportEventWithParameters(eventType, params)
}
