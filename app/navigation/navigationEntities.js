// @flow

import * as React from 'react'

import { type RouteName } from './SwotListNavigator'

export type NavigationOptions = {|
    +title: string,
    +headerRight?: React.Node,
    +headerTitleStyle?: {},
|}

export type NavigationEvent = 'willFocus' | 'didFocus' | 'willBlur' | 'didBlur'

export type NavigationEventListener = {|
    +remove: () => void,
|}

export type Navigation<T> = {|
    +state: { params: T },
    +setParams: (param: T) => void,
    +navigate: (routeName: RouteName, param?: T) => void,
    +addListener: (
        name: NavigationEvent,
        handler: Function
    ) => NavigationEventListener,
|}

export type NavigationWrapper<T> = {|
    +navigation: Navigation<T>,
|}

export type NavigationOptionsFunction<T> = (
    navigation: NavigationWrapper<T>
) => NavigationOptions

export type NavigateFunction<T> = (
    navigation: Navigation<T>
) => (param: T) => any

export function makeNavigationOptions<T>(
    makeNavOptions: (params: T) => NavigationOptions
) {
    function inner(navigationWrapper: NavigationWrapper<T>) {
        const { navigation } = navigationWrapper
        const { state } = navigation
        const { params } = state
        return makeNavOptions(params)
    }
    return inner
}
