import sys
import os
import json
from os import walk

print sys.argv

configuration = sys.argv[1]
libsTempOutput = sys.argv[2]
output = sys.argv[3]
subprojRoot = sys.argv[4]

noTarget = 'NO_TAGRET'

def getTargetName(dictionary):

    targetExceptions = (
    '-tvOS', # use mobile targets only
    'Tests', # do not use test targets
    'HelloWorld', # do not use test targets
    'fishhook' # dependency of RCTWebSocket
    )

    for k in dictionary['objects']:
        node = dictionary['objects'][k]
        if node['isa'] == 'PBXNativeTarget':
            name = node['name']
            if not name.endswith(targetExceptions):
                return name

    return noTarget

def build(subprojPath):
    pbxPath = subprojPath + "/project.pbxproj"
    tempJsonPath = subprojPath + "/Data.json"

    convertToJson =  "plutil -convert json -o {0} {1}".format(tempJsonPath, pbxPath)
    os.system(convertToJson)

    with open(tempJsonPath, 'r') as f:
        data = json.load(f)

    os.remove(tempJsonPath)

    targetName = getTargetName(data)

    if targetName == noTarget:
        return

    outputLibName = "lib" + targetName
    buildSubproject = "sh build_lib_react.sh {0} {1} {2} {3} {4} {5}".format(configuration, outputLibName, targetName, subprojPath, libsTempOutput, output)
    os.system(buildSubproject)


print "*** BUILD REACT SATELLITE LIBS ***"
for (dirpath, dirnames, filenames) in walk(subprojRoot):

    projectExceptions = (
    'React.xcodeproj', # React core should be builded before
    'Sample.xcodeproj', # unuseful in your app
    'RCTTest.xcodeproj', # unuseful in your app (may be)
    'Tests.xcodeproj' # do not use test propjects
    )

    if not dirpath.endswith(projectExceptions):
        if 'project.pbxproj' in filenames:
            build(dirpath)


print "*** MERGE REACT SATELLITE LIBS ***"
satelliteLibs = ""
for (dirpath, dirnames, filenames) in walk(libsTempOutput):
    for libFile in filenames:
        if libFile.endswith(".a"):
            satelliteLibs = satelliteLibs + " " + dirpath +  "/" + libFile
mergeLibs = "libtool -static -o " + output + "/libReactIntegrated.a" + satelliteLibs
print satelliteLibs
os.system(mergeLibs)
