// @flow

// Library tools
import React from 'react'
import { View } from 'react-native'

// RN issue: https://github.com/facebook/react-native/issues/15707

type Props = {|
    +height: number,
|}

export default ({ height }: Props) => {
    const style = { height: 0, marginBottom: height }
    return <View style={style} />
}
