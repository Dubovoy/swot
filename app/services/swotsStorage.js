// @flow strict

import { type SQLiteDatabase, ResultSet } from 'react-native-sqlite-storage'

import {
    getDBConnection,
    performReadRequest,
    performWriteRequest,
} from './storageCore'

import {
    type SectionName,
    type Swot,
    type SwotItem,
    type Question,
} from '../entities/model'

const swotDBName = 'swots'
// тильда означает, что файл БД будет считываться из бандла приложения.
// При первом запуске файл будет автоматически скопирован в каталог ~/Library/LocalDatabase/
const swotDBPath = '~/swots'

const Storage: {
    connection: ?SQLiteDatabase,
    getConnection: () => Promise<?SQLiteDatabase>,
} = {
    connection: null,
    async getConnection() {
        if (this.connection) {
            return this.connection
        }
        const db = await getDBConnection(swotDBName, swotDBPath)
        this.connection = db
        return db
    },
}

export async function addNewSwotIntoStorage(title: string): Promise<void> {
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return
    const query = `INSERT INTO swots(title) VALUES('${title}')`
    await performWriteRequest(db, query)
}

export async function deleteSwotFromStorage(id: number): Promise<void> {
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return
    const query = `DELETE FROM swots WHERE id = ${id}`
    await performWriteRequest(db, query)
}

export async function getSwotListFromStorage(): Promise<Array<Swot>> {
    const emptyList = []
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return emptyList
    let results: ?ResultSet
    try {
        results = await performReadRequest(db, 'SELECT * FROM swots')
    } catch (ex) {
        console.error(ex)
    }
    if (!results) return emptyList
    const len = results.rows.length
    const swotsArray = new Array<Swot>(len)
    for (let i = 0; i < len; i++) {
        const { id, title } = results.rows.item(i)
        swotsArray[i] = { id, title }
    }

    return swotsArray
}

export async function getSwotTitle(id: number): Promise<?string> {
    const emptyValue = null
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return emptyValue
    let results: ?ResultSet
    try {
        results = await performReadRequest(
            db,
            `SELECT title FROM swots WHERE id = ${id}`
        )
    } catch (ex) {
        console.error(ex)
    }
    if (!results) return emptyValue

    const len = results.rows.length
    let resultValue = emptyValue
    for (let i = 0; i < len; i++) {
        const { title } = results.rows.item(i)
        resultValue = title
        break
    }

    return resultValue
}
export async function updateSwotItemInStorage(
    swotItemId: number,
    text: string
): Promise<void> {
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return
    const query = text
        ? `UPDATE swotItems SET text = '${text}' WHERE id = '${swotItemId}'`
        : `DELETE FROM swotItems WHERE id = ${swotItemId}`
    await performWriteRequest(db, query)
}

export async function addNewSwotItemIntoStorage(
    swotId: number,
    section: SectionName,
    text: string,
    questionId: number
): Promise<void> {
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return
    const query = `INSERT INTO swotItems(swotId, section, text, questionId) VALUES('${swotId}','${section}','${text}','${questionId}')`
    await performWriteRequest(db, query)
}

export async function deleteSwotItemFromStorage(id: number): Promise<void> {
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return
    const query = `DELETE FROM swotItems WHERE id = ${id}`
    await performWriteRequest(db, query)
}

export async function getSwotItemFromStorage(
    swotItemId: number
): Promise<?SwotItem> {
    const emptyValue = null
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return emptyValue
    let results: ?ResultSet
    try {
        results = await performReadRequest(
            db,
            `SELECT * FROM swotItems WHERE id=${swotItemId}`
        )
    } catch (ex) {
        console.error(ex)
    }
    if (!results) return emptyValue
    const len = results.rows.length
    let resultValue = emptyValue
    for (let i = 0; i < len; i++) {
        const { id, swotId, section, text, questionId } = results.rows.item(i)
        resultValue = { id, swotId, section, text, questionId }
        break
    }

    return resultValue
}

export async function getSwotItemListFromStorage(
    swotId: number,
    sectionParam?: SectionName
): Promise<Array<SwotItem>> {
    const emptyList = []
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return emptyList
    let results: ?ResultSet
    try {
        const sectionContition = sectionParam
            ? ` AND section='${sectionParam}'`
            : ''
        results = await performReadRequest(
            db,
            `SELECT * FROM swotItems WHERE swotId=${swotId}${sectionContition}`
        )
    } catch (ex) {
        console.error(ex)
    }
    if (!results) return emptyList
    const len = results.rows.length
    const swotItemsArray = new Array<SwotItem>(len)
    for (let i = 0; i < len; i++) {
        const { id, section, text, questionId } = results.rows.item(i)
        swotItemsArray[i] = { id, swotId, section, text, questionId }
    }

    return swotItemsArray
}

export async function getQuestionListFromStorage(
    packIDs: Array<number>,
    sectionParam?: SectionName
): Promise<Array<Question>> {
    const emptyList = []
    const db: ?SQLiteDatabase = await Storage.getConnection()
    if (!db) return emptyList
    let results: ?ResultSet
    try {
        const sectionContition = sectionParam
            ? ` AND section='${sectionParam}'`
            : ''
        const packCondition = packIDs.join()
        results = await performReadRequest(
            db,
            `SELECT * FROM questions_en WHERE packId IN (${packCondition})${sectionContition}`
        )
    } catch (ex) {
        console.error(ex)
    }
    if (!results) return emptyList
    const len = results.rows.length
    const questionsArray = new Array<Question>(len)
    for (let i = 0; i < len; i++) {
        const { id, packId, text, section } = results.rows.item(i)
        questionsArray[i] = { id, packId, section, text }
    }

    return questionsArray
}
