// @flow

import { type RootState } from '../rootReducer'

// Types

export type OnboardingState = {|
    +wasShown: boolean,
|}

export type setWasShownAction = {|
    +type: 'SET_WAS_SHOWN',
    +payload: {|
        +wasShown: boolean,
    |},
|}

// Reducer
const defaultState: OnboardingState = {
    wasShown: false,
}

export default function reducer(
    state: $Exact<OnboardingState> = defaultState,
    action: setWasShownAction
): $Exact<OnboardingState> {
    switch (action.type) {
        case 'SET_WAS_SHOWN': {
            const { wasShown } = action.payload
            return {
                ...state,
                wasShown,
            }
        }
        default: {
            return state
        }
    }
}

// Action creators
export const setWasShown = (wasShown: boolean): setWasShownAction => {
    const type = 'SET_WAS_SHOWN'
    return { type, payload: { wasShown } }
}

// Selectors
export const getWasShown = (state: RootState): boolean =>
    state.onboarding.wasShown
