// @flow

import { createStackNavigator } from 'react-navigation'
import SwotListScreen from '../features/swotList/SwotListScreen'
import SwotScreen from '../features/currentSwot/SwotScreen'
import AnswersScreen from '../features/answers/AnswersScreen'

import palette from '../styles/palette'
import typography from '../styles/typography'

export const Screens = {
    SwotList: 'SwotListScreen',
    Swot: 'SwotScreen',
    Answers: 'AnswersScreen',
}

export type RouteName = 'SwotListScreen' | 'SwotScreen' | 'AnswersScreen'

export default createStackNavigator(
    {
        [Screens.SwotList]: {
            screen: SwotListScreen,
        },
        [Screens.Swot]: {
            screen: SwotScreen,
        },
        [Screens.Answers]: {
            screen: AnswersScreen,
        },
    },
    {
        initialRouteName: Screens.SwotList,
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: palette.navbar,
            },
            headerBackTitle: null,
            headerTintColor: palette.contrastLightText,
            headerTitleStyle: {
                fontWeight: typography.fontWeight.header,
            },
        },
    }
)
