// @flow

// Library tools
import * as React from 'react'
import { Platform } from 'react-native'

// Library components
import Dialog from 'react-native-dialog'
import { TextField } from 'react-native-material-textfield'

// Styles
import palette from '../styles/palette'

type Props = {
    onChangeText: (text: string) => void,
    placeholder: string,
}

export default ({ onChangeText, placeholder }: Props) =>
    Platform.select({
        ios: (
            <Dialog.Input
                placeholder={placeholder}
                onChangeText={onChangeText}
            />
        ),
        android: (
            <TextField
                label={placeholder}
                onChangeText={onChangeText}
                tintColor={palette.navbar}
            />
        ),
    })
