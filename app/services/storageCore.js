// @flow strict

import SQLite, {
    type SQLiteDatabase,
    type DatabaseParams,
    type SQLError,
    type Transaction,
    type StatementCallback,
    type StatementErrorCallback,
    type ResultSet,
} from 'react-native-sqlite-storage'

export async function getDBConnection(
    dbName: string,
    dbPath: string,
    readOnly: boolean = false
): Promise<?SQLiteDatabase> {
    const readWriteParams = {
        name: dbName,
        createFromLocation: dbPath,
    }
    const readOnlyParams = {
        name: dbName,
        createFromLocation: dbPath,
        // readOnly param enables readOnly-mode regardless of the param value
        readOnly,
    }

    const dbParams: DatabaseParams = readOnly ? readOnlyParams : readWriteParams

    // method does no throw exceptions so there is no sense to use try/catch
    const db = await SQLite.openDatabase(dbParams)
    return db
}

export async function performReadRequest(
    db: SQLiteDatabase,
    query: string
): Promise<ResultSet> {
    return new Promise((resolve, reject) => {
        const successCallback: StatementCallback = (
            tx: Transaction,
            results: ResultSet
        ) => {
            resolve(results)
        }

        const errorCallback: StatementErrorCallback = (err: SQLError) => {
            reject(err)
        }

        db.readTransaction(tx => {
            tx.executeSql(query, [], successCallback, errorCallback)
        })
    })
}

export async function performWriteRequest(
    db: SQLiteDatabase,
    query: string
): Promise<void> {
    return new Promise((resolve, reject) => {
        const successCallback: StatementCallback = () => resolve()

        const errorCallback: StatementErrorCallback = (err: SQLError) => {
            reject(err)
        }
        db.sqlBatch([query], successCallback, errorCallback)
    })
}

// export async function testDBConnection(dbName: string, dbPath: string) {
//     const queryParser: StatementCallback = (
//         tx: Transaction,
//         results: ResultSet
//     ) => {
//         console.log('Query completed')
//         console.log(' ========== TABLE NAMES ==========')
//         const len = results.rows.length
//         for (let i = 0; i < len; i++) {
//             const row = results.rows.item(i)
//             console.log(row)
//         }
//     }

//     function errorCallback(err: SQLError): void {
//         console.log(`${dbName} database opening error: ${err.message}`)
//     }

//     function successCallback(): void {
//         console.log(`${dbName} database opened`)
//     }

//     const dbParams: DatabaseParams = {
//         name: dbName,
//         createFromLocation: dbPath,
//         readOnly: true,
//     }

//     let DB: ?SQLiteDatabase = null
//     try {
//         DB = await SQLite.openDatabase(dbParams, successCallback, errorCallback)
//     } catch (ex) {
//         console.log(ex)
//     }
//     if (!DB) return

//     let result
//     try {
//         result = await DB.readTransaction(tx => {
//             tx.executeSql(
//                 "SELECT name FROM sqlite_master WHERE type='table'",
//                 [],
//                 queryParser
//             )
//         })
//     } catch (ex) {
//         console.log(ex)
//     }
//     if (!result) return
//     console.log(result)
// }
