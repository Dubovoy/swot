CONFIGURATION=$1

LIBS_OUTPUT="libs_${CONFIGURATION}"
LIBS_TEMP_OUTPUT="${PWD}/libsTemp_${CONFIGURATION}"
OUTPUT="${PWD}/libReactIntegrated_${CONFIGURATION}"
SUBPROJ_ROOT="../node_modules"


# clean up
rm -r ../node_modules
npm ci ../

rm -r $LIBS_OUTPUT
rm -r $LIBS_TEMP_OUTPUT
rm -r $OUTPUT
mkdir -p "${LIBS_OUTPUT}"
mkdir -p "${LIBS_TEMP_OUTPUT}"
mkdir -p "${OUTPUT}"

# build core react
sh build_lib_react.sh $CONFIGURATION libReact React "../node_modules/react-native/React/React.xcodeproj" $LIBS_TEMP_OUTPUT $OUTPUT


# add react header search path into all react satellites (xCode subprojects) at node_modules
python addHeaderSearchPathToReactSatellites.py "${PWD}/${LIBS_OUTPUT}/libReact/universal/include/"


# build merged react lib
python buildSubprojects.py $CONFIGURATION "${LIBS_TEMP_OUTPUT}" "${OUTPUT}" "${SUBPROJ_ROOT}"
rm -r $LIBS_TEMP_OUTPUT
