// @flow

import { type SectionName, type SwotItem } from '../../entities/model'

// adds RN SectionList compatinility
type ReactSectionListItem = {
    [key: string]: any,
}

export type SwotSection = {|
    +name: SectionName,
    +data: Array<SwotItem>,
    +isMinimized: boolean,
    +isEmpty: boolean,
|} & ReactSectionListItem
