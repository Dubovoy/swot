// @flow

// Library tools
import * as React from 'react'
import { connect } from 'react-redux'

// Library components
import { Text, SafeAreaView, ScrollView, View } from 'react-native'
import { WaveIndicator } from 'react-native-indicators'
import { NavigationEvents } from 'react-navigation'

// App components
import PurchaseList from './PurchaseList'
import withTouchHandlers from '../../components/HOCs/withTouchHandlers'

// Styles
import layouts from '../../styles/layouts'
import palette from '../../styles/palette'

// Locales
import { localizedString } from '../../locales/localizedString'

// Business logic
import {
    restoreAllPurchasesAsync,
    updateProductsAsync,
} from './purchaseAsyncActions'

import { getIsRestoreFetching } from './purchase'

// Utils
import { reportEvent } from '../../services/metrica'

// Entities
import { type RootState } from '../rootReducer'

type StateMapProps = {|
    +isRestoreFetching: boolean,
|}

type DispatchMapProps = {|
    +restore: () => {},
    +update: () => void,
|}

type Props = StateMapProps & DispatchMapProps

class QuestionPacksScreen extends React.Component<Props> {
    static navigationOptions = () => {
        return {
            title: localizedString('questionsScreen.title'),
        }
    }

    componentDidMount() {
        const { update } = this.props
        update()
    }

    get restoreButton() {
        const { isRestoreFetching, restore } = this.props
        const pressHandler = { senderKey: 0, onPress: restore }

        const RestoreButtonContent = () => (
            <View style={layouts.button.restoreButton}>
                {isRestoreFetching ? (
                    <WaveIndicator color={palette.contrastLightText} />
                ) : (
                    <Text style={layouts.basicText.lightText}>
                        {localizedString('questionPacksScreen.restore')}
                    </Text>
                )}
            </View>
        )
        // TODO: разобраться, почему здесь flow глючит
        // eslint-disable-next-line flowtype-errors/show-errors
        const InteractiveButton = withTouchHandlers(pressHandler)(
            RestoreButtonContent
        )
        return <InteractiveButton />
    }

    render(): React.Element<any> {
        return (
            <SafeAreaView style={layouts.containers.allAvailableSpace}>
                <NavigationEvents
                    onWillFocus={() => {
                        reportEvent({
                            questionPacksScreen: {
                                viewShown: 'questionPacksScreen',
                            },
                        })
                    }}
                />
                <ScrollView>
                    <PurchaseList env="questionsTab" />
                    {this.restoreButton}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state: RootState): StateMapProps => ({
    isRestoreFetching: getIsRestoreFetching(state),
})

const mapDispatchToProps = (dispatch): DispatchMapProps => ({
    restore: () => dispatch(restoreAllPurchasesAsync()),
    update: () => dispatch(updateProductsAsync()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(QuestionPacksScreen)
