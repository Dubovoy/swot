// @flow

import { StyleSheet } from 'react-native'

import palette from './palette'
import typography from './typography'

const containers = StyleSheet.create({
    allAvailableSpace: {
        alignSelf: 'stretch',
        flex: 1,
    },
    answerField: {
        marginBottom: 10,
        minHeight: typography.fontSize.textBlock * 3,
        padding: 10,
        width: '100%',
    },
    centeredContainer: {
        alignItems: 'center',
        alignSelf: 'stretch',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    onboardingImage: {
        height: 200,
        marginTop: 20,
        width: 235,
    },
    paginationContainer: {
        alignItems: 'center',
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    sideButtonContainer: {
        width: '25%',
    },
    stretchedContainer: {
        alignItems: 'stretch',
        alignSelf: 'stretch',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
})

const basicText = StyleSheet.create({
    answerField: {
        borderColor: palette.border,
        borderRadius: 5,
        borderWidth: 2,
        color: palette.mainText,
        flex: 1,
        fontSize: typography.fontSize.textBlock,
        marginBottom: 10,
        minHeight: typography.fontSize.textBlock * 3,
        padding: 10,
        width: '100%',
    },
    greenText: {
        color: palette.greenButton,
        fontSize: typography.fontSize.itemTitle,
    },
    header: {
        color: palette.mainText,
        fontSize: 24,
        fontWeight: 'bold',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        textAlign: 'center',
    },
    lightText: {
        color: palette.contrastLightText,
        fontSize: typography.fontSize.itemTitle,
    },
    messageText: {
        fontSize: 24,
    },
    questionContainer: {
        color: palette.mainText,
        fontSize: typography.fontSize.itemTitle,
        fontStyle: 'italic',
        marginBottom: 20,
        marginTop: 20,
    },
    regular: {
        color: palette.mainText,
        fontSize: 16,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        textAlign: 'center',
    },
})

const button = StyleSheet.create({
    availablePanel: {
        alignItems: 'center',
        borderColor: palette.border,
        borderRadius: 10,
        borderWidth: 2,
        flexDirection: 'row',
        height: 44,
        justifyContent: 'center',
    },
    buyButton: {
        alignItems: 'center',
        backgroundColor: palette.blueButton,
        borderRadius: 10,
        flexDirection: 'row',
        height: 44,
        justifyContent: 'center',
    },
    buyButtonFree: {
        alignItems: 'center',
        backgroundColor: palette.greenButton,
        borderRadius: 10,
        flexDirection: 'row',
        height: 44,
        justifyContent: 'center',
    },
    restoreButton: {
        alignItems: 'center',
        backgroundColor: palette.greenButton,
        borderRadius: 10,
        flexDirection: 'row',
        height: 44,
        justifyContent: 'center',
        margin: 20,
        marginLeft: 10,
        marginRight: 10,
    },
})

const listItem = StyleSheet.create({
    container: {
        backgroundColor: palette.mainBack,
        justifyContent: 'center',
        minHeight: 44,
        paddingBottom: 8,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 8,
    },
    separator: {
        backgroundColor: palette.border,
        height: 1,
    },
    subtitle: {
        color: palette.mainText,
        fontSize: typography.fontSize.itemSubtitle,
        fontWeight: typography.fontWeight.itemSubtitle,
    },
    text: {
        color: palette.mainText,
        fontSize: typography.fontSize.textBlock,
        fontWeight: typography.fontWeight.textBlock,
    },
    title: {
        color: palette.mainText,
        fontSize: typography.fontSize.itemTitle,
        fontWeight: typography.fontWeight.itemTitle,
    },
})

const listSectionHeader = StyleSheet.create({
    container: {
        alignItems: 'center',
        borderColor: palette.border,
        borderRadius: 10,
        flexDirection: 'row',
        height: 44,
        justifyContent: 'space-between',
        margin: 4,
    },
    leftButton: {
        alignItems: 'center',
        height: 44,
        justifyContent: 'center',
        width: 44,
    },
    rightButton: {
        alignItems: 'center',
        height: 44,
        justifyContent: 'center',
        width: 44,
    },
    text: {
        color: palette.mainText,
        fontSize: typography.fontSize.header,
        fontWeight: typography.fontWeight.header,
    },
})

export default {
    containers,
    basicText,
    button,
    listItem,
    listSectionHeader,
}
