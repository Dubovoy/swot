// @flow

import {
    addNewSwotIntoStorage,
    deleteSwotFromStorage,
    getSwotListFromStorage,
} from '../../services/swotsStorage'

import {
    type UpdateActionA,
    type UpdateActionB,
    updateSwotList,
} from './swotList'

import { type GetState } from '../rootReducer'

import { type Swot } from '../../entities/model'

type Action = UpdateActionA | UpdateActionB
type Dispatch = (action: Action) => void
type ThunkAction = (dispatch: Dispatch, getState: GetState) => Promise<void>

export function addNewSwot(title: string): ThunkAction {
    return async (dispatch: Dispatch) => {
        await addNewSwotIntoStorage(title)
        const updatedSwotList: Array<Swot> = await getSwotListFromStorage()
        dispatch(updateSwotList(updatedSwotList))
    }
}

export function deleteSwot(id: number): ThunkAction {
    return async (dispatch: Dispatch) => {
        await deleteSwotFromStorage(id)
        const updatedSwotList: Array<Swot> = await getSwotListFromStorage()
        dispatch(updateSwotList(updatedSwotList))
    }
}
