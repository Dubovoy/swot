// @flow

// Library tools
import React from 'react'
import { SafeAreaView } from 'react-native'

// App components
import PurchaseList from '../features/purchases/PurchaseList'

// Styles
import layouts from '../styles/layouts'

// Entities

type Props = {|
    +env: string,
|}

const MoreQuestionsView = ({ env }: Props) => {
    return (
        <SafeAreaView style={layouts.containers.allAvailableSpace}>
            <PurchaseList env={`moreQuestionsView+${env}`} />
        </SafeAreaView>
    )
}

export default MoreQuestionsView
