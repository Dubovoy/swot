// @flow

import { store } from '../store'
import { updateSwotList } from './swotList/swotList'
import { getSwotListFromStorage } from '../services/swotsStorage'

import { type Swot } from '../entities/model'

export async function initReduxStore() {
    // initial sync between prepopulated SQLite and redux persist
    const updatedSwotList: Array<Swot> = await getSwotListFromStorage()
    store.dispatch(updateSwotList(updatedSwotList))
}
