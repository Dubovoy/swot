// @flow strict

const palette = {
    background: '#F5F5F5',
    main: '#34597A',
    borderGrey: 'lightgrey',
    contrastLight: '#F5F5F5',
    contrastDark: '#354757',
    alertRed: 'rgb(255, 60, 60)',

    strengthses: '#53900f',
    weaknesses: '#FFC000',
    opportunities: '#5B9BD5',
    threats: '#ED7D31',
}

export default {
    navbar: palette.main,
    mainText: palette.contrastDark,
    contrastLightText: palette.contrastLight,
    mainBack: palette.background,
    headerBackColorS: palette.strengthses,
    headerBackColorW: palette.weaknesses,
    headerBackColorO: palette.opportunities,
    headerBackColorT: palette.threats,
    border: palette.borderGrey,
    deleteRed: palette.alertRed,
    blueButton: palette.main,
    greenButton: palette.strengthses,
}
