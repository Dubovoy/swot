// @flow
declare module 'react-native-sqlite-storage' {
    declare export interface DatabaseParams {
        name: string;
        createFromLocation?: string;
        readOnly?: boolean;
    }

    declare export interface ResultSetRowList {
        length: number;
        item(index: number): any;
    }

    declare export interface ResultSet {
        insertId: number;
        rowsAffected: number;
        rows: ResultSetRowList;
    }

    declare export interface SQLError {
        code: number;
        message: string;
    }

    declare export interface Transaction {
        executeSql(sqlStatement: string): Promise<[Transaction, ResultSet]>;
        executeSql(
            statement: string,
            params?: any[],
            success?: StatementCallback,
            error?: StatementErrorCallback
        ): void;
    }

    declare export type TransactionCallback = (transaction: Transaction) => void

    declare export interface SQLiteDatabase {
        readTransaction(
            scope: (tx: Transaction) => void
        ): Promise<TransactionCallback>;

        sqlBatch(
            queries: Array<string>,
            success?: StatementCallback,
            error?: StatementErrorCallback
        ): Promise<void>;
    }

    declare export type StatementErrorCallback = (error: SQLError) => void

    declare export type StatementCallback = (
        transaction: Transaction,
        resultSet: ResultSet
    ) => void

    declare export class SQLite {
        static openDatabase(params: DatabaseParams): SQLiteDatabase;
    }

    declare export default typeof SQLite
}
