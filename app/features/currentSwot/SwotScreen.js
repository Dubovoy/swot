// @flow

// Library tools
import * as React from 'react'
import { connect } from 'react-redux'

// Library components
import {
    Dimensions,
    LayoutAnimation,
    SafeAreaView,
    SectionList,
    View,
} from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

// App components
import { makeSwotSectionHeaderView } from '../../components/swotSection/SwotSectionHeaderView'
import { makeSwotItemView } from '../../components/swotSection/SwotItemView'
import FlatListFooterStub from '../../components/FlatListFooterStub'
import withTouchHandlers from '../../components/HOCs/withTouchHandlers'

// Styles
import layouts from '../../styles/layouts'
import palette from '../../styles/palette'

// Locales
// import { localizedString } from '../../locales/localizedString'

// Business logic
import {
    getSwotItemList,
    getSwotId,
    getSwotTitle,
    switchSectionCollapseState,
} from './swot'
import { deleteSwotItem, shareAsPdf } from './swotAsyncActions'
import { setSwotInfoAsync } from '../answers/answersAsyncActions'

// Entities
import { type SectionName, type SwotItem } from '../../entities/model'
import { type SwotSection } from '../../components/swotSection/SwotSection'
import { type RootState } from '../rootReducer'

// Navigation
import {
    type Navigation,
    makeNavigationOptions,
} from '../../navigation/navigationEntities'

type InitalNavData = {|
    +title: string,
    +headerTitleStyle?: {},
|}

type StateMapProps = {|
    +id: number,
    +title: string,
    +swotItems: $ReadOnlyArray<SwotSection>,
|}

type DispatchMapProps = {|
    +onSectionPressed: (sectionName: SectionName) => void,
    +onAddButtonPressed: (swotId: number, sectionName: SectionName) => void,
    +onListItemPressed: (swotItem: SwotItem) => void,
    +onListItemLongPressed: (swotItem: SwotItem) => void,
    +onShare: (swotId: number, title: string) => void,
|}

type NavigationProps = {|
    +navigation: Navigation<InitalNavData>,
|}

type Props = StateMapProps & DispatchMapProps & NavigationProps

type State = {|
    +swotItems: Array<Object>,
|}

const makeHeaderTitleStyle = () => ({
    width: Dimensions.get('window').width - 44 * 2,
})

class SwotScreen extends React.Component<Props, State> {
    static navigationOptions = makeNavigationOptions<InitalNavData>(params => {
        const title = params ? params.title : ''
        // const onAddSwot = params ? params.onAddSwot : null

        const AddButtonShape = () => (
            <View style={layouts.listSectionHeader.rightButton}>
                <Icon
                    name="share"
                    size={25}
                    color={palette.contrastLightText}
                />
            </View>
        )
        const AddButton = withTouchHandlers({
            senderKey: 0,
            onPress: () => {
                // eslint-disable-next-line flowtype-errors/show-errors
                params.onShare()
            },
        })(AddButtonShape)

        return {
            title,
            headerTitleStyle: makeHeaderTitleStyle(),
            headerRight: <AddButton />,
        }
    })

    componentDidMount() {
        const { navigation, title } = this.props
        const headerTitleStyle = makeHeaderTitleStyle()
        // eslint-disable-next-line flowtype-errors/show-errors
        navigation.setParams({ title, headerTitleStyle, onShare: this.onShare })
    }

    onShare = async () => {
        const { id, onShare, title } = this.props
        onShare(id, title)
    }

    collapse = ({ sectionName }) => {
        const { onSectionPressed } = this.props
        LayoutAnimation.easeInEaseOut()
        onSectionPressed(sectionName)
    }

    addSwotItem = (sectionName: SectionName) => {
        const { id, onAddButtonPressed } = this.props
        onAddButtonPressed(id, sectionName)
    }

    openSwotItem = (swotItem: SwotItem) => {
        const { onListItemPressed } = this.props
        onListItemPressed(swotItem)
    }

    deleteSwotItem = (swotItem: SwotItem) => {
        const { onListItemLongPressed } = this.props
        onListItemLongPressed(swotItem)
    }

    renderItem = makeSwotItemView(this.openSwotItem, this.deleteSwotItem)

    renderSectionHeader = makeSwotSectionHeaderView(
        this.addSwotItem,
        this.collapse
    )

    separator = (): React.Element<any> => (
        <View style={layouts.listItem.separator} />
    )

    keyExtractor = (item: SwotItem): string => String(item.id)

    render(): React.Element<any> {
        const { swotItems } = this.props
        return (
            <SafeAreaView style={layouts.containers.allAvailableSpace}>
                <SectionList
                    renderItem={this.renderItem}
                    renderSectionHeader={this.renderSectionHeader}
                    ItemSeparatorComponent={this.separator}
                    sections={swotItems}
                    keyExtractor={this.keyExtractor}
                    ListFooterComponent={<FlatListFooterStub height={0} />}
                />
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state: RootState): StateMapProps => ({
    id: getSwotId(state),
    title: getSwotTitle(state),
    swotItems: getSwotItemList(state),
})

const mapDispatchToProps = (
    dispatch,
    ownProps: NavigationProps
): DispatchMapProps => ({
    onListItemPressed: swotItem => {
        dispatch(
            setSwotInfoAsync(swotItem.swotId, swotItem.section, swotItem.id)
        ).then(() => {
            ownProps.navigation.navigate('AnswersScreen')
        })
    },
    onListItemLongPressed: swotItem => {
        dispatch(deleteSwotItem(swotItem.id))
    },
    onAddButtonPressed: (swotItemId, sectionName) => {
        dispatch(setSwotInfoAsync(swotItemId, sectionName)).then(() => {
            ownProps.navigation.navigate('AnswersScreen', {
                title: sectionName,
            })
        })
    },
    onSectionPressed: sectionName =>
        dispatch(switchSectionCollapseState(sectionName)),
        onShare: (swotId, title) => dispatch(shareAsPdf(swotId, title)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SwotScreen)
