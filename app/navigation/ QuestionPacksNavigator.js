// @flow

import { createStackNavigator } from 'react-navigation'
import QuestionPacksScreen from '../features/purchases/QuestionPacksScreen'

import palette from '../styles/palette'
import typography from '../styles/typography'

export const Screens = {
    Purchase: 'QuestionPacksScreen',
}

export type RouteName = 'QuestionPacksScreen'

export default createStackNavigator(
    {
        [Screens.Purchase]: {
            screen: QuestionPacksScreen,
        },
    },
    {
        initialRouteName: Screens.Purchase,
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: palette.navbar,
            },
            headerBackTitle: null,
            headerTintColor: palette.contrastLightText,
            headerTitleStyle: {
                fontWeight: typography.fontWeight.header,
            },
        },
    }
)
