// @flow

// Library tools
import * as React from 'react'
import { connect } from 'react-redux'
import deepEqual from 'deep-equal'

// Library components
import { Dimensions, Keyboard, SafeAreaView } from 'react-native'
import Carousel from 'react-native-snap-carousel'

// App components
import QuestionView from '../../components/QuestionView'
import MoreQuestionsView from '../../components/MoreQuestionsView'

// Styles
import layouts from '../../styles/layouts'

// Business logic
import {
    getSectionName,
    getFirstCardIndex,
    updateAnswerChanges,
    getCards,
    getEditedAnswer,
} from './answers'
import { updateProductsAsync } from '../purchases/purchaseAsyncActions'

import { updateCardsAsync } from './answersAsyncActions'
import { applyAnswerChangesAsync } from '../currentSwot/swotAsyncActions'

import { getPurchasedPacks } from '../purchases/purchase'

// Utils
import { reportEvent } from '../../services/metrica'

// Entities
import { type RootState } from '../rootReducer'
import {
    CarouselCard,
    QuestionCard,
    MoreQuestionsCard,
} from '../../entities/viewModel'

// Navigation
import {
    type Navigation,
    type NavigationEventListener,
    makeNavigationOptions,
} from '../../navigation/navigationEntities'

type InitalNavData = {|
    title: string,
|}

type StateMapProps = {|
    firstCardIndex: number,
    sectionName: string,
    cards: Array<CarouselCard>,
    // eslint-disable-next-line react/no-unused-prop-types
    packIds: Array<number>,
|}

type DispatchMapProps = {|
    onGoBack: () => void,
    updateProducts: () => void,
    makeAutoActions: (props: StateMapProps, prevProps: StateMapProps) => void,
|}

type NavigationProps = {|
    navigation: Navigation<InitalNavData>,
|}

type Props = StateMapProps & DispatchMapProps & NavigationProps

type State = {|
    width: number,
|}

class AnswersScreen extends React.Component<Props, State> {
    static navigationOptions = makeNavigationOptions<InitalNavData>(params => {
        const title = params ? params.title : ''
        return {
            title,
        }
    })

    // TODO: connect-ы нужно уносить внутрь компомнента, и типизировать
    // точно так же, как типизируем connect для экрана, с Union
    // различных типов
    connectToEditings = connect(
        (state, ownProps) => ({
            ...ownProps,
            editedAnswerText: getEditedAnswer(state, ownProps.questionId),
        }),
        dispatch => ({
            editAnswer: (questionId, answerText) =>
                dispatch(updateAnswerChanges(questionId, answerText)),
        })
    )

    constructor(props) {
        super(props)
        const { width } = Dimensions.get('window')
        this.state = { width }
    }

    componentDidMount() {
        const { navigation, sectionName } = this.props
        navigation.setParams({ title: sectionName })
        this.goBackListener = navigation.addListener('willBlur', () => {
            const { onGoBack } = this.props
            onGoBack()
        })

        const { cards, updateProducts } = this.props
        if (cards.length === 1 /* purchases card only */) {
            updateProducts()
        }
    }

    componentDidUpdate(prevProps) {
        const { makeAutoActions } = this.props
        makeAutoActions(this.props, prevProps)
    }

    componentWillUnmount() {
        this.goBackListener.remove()
    }

    onLayout = () => {
        const { width } = Dimensions.get('window')
        this.setState({ width })
    }

    onBeforeSnapToItem = slideIndex => {
        const { cards, updateProducts } = this.props
        if (slideIndex === cards.length - 1) {
            reportEvent({ answersScreen: { viewShown: 'moreQuestionsCard' } })
            updateProducts()
        }
        Keyboard.dismiss()
    }

    goBackListener: NavigationEventListener

    renderItem = ({ item }) => {
        if (item instanceof QuestionCard) {
            const EditableCarouselCard = this.connectToEditings(QuestionView)
            return (
                <EditableCarouselCard
                    questionText={item.question.text}
                    answerText={item.swotItem ? item.swotItem.text : ''}
                    questionId={item.question.id}
                />
            )
        }
        if (item instanceof MoreQuestionsCard) {
            const { sectionName } = this.props
            return <MoreQuestionsView env={sectionName} />
        }
        return null
    }

    render(): React.Element<any> {
        const { firstCardIndex, cards } = this.props
        const { width } = this.state
        return (
            <SafeAreaView
                onLayout={this.onLayout}
                style={layouts.containers.allAvailableSpace}
            >
                <Carousel
                    // https://github.com/archriss/react-native-snap-carousel/issues/145
                    removeClippedSubviews={false}
                    data={cards}
                    renderItem={this.renderItem}
                    firstItem={firstCardIndex}
                    sliderWidth={width}
                    itemWidth={width - 40}
                    onBeforeSnapToItem={this.onBeforeSnapToItem}
                />
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state: RootState): StateMapProps => ({
    firstCardIndex: getFirstCardIndex(state),
    sectionName: getSectionName(state),
    cards: getCards(state),
    packIds: getPurchasedPacks(state),
})

const mapDispatchToProps = (dispatch): DispatchMapProps => ({
    onGoBack: () => dispatch(applyAnswerChangesAsync()),
    makeAutoActions: (props: StateMapProps, prevProps: StateMapProps) => {
        if (!deepEqual(props.packIds, prevProps.packIds)) {
            dispatch(updateCardsAsync())
        }
    },
    updateProducts: () => {
        dispatch(updateProductsAsync())
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AnswersScreen)
