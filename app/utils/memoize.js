// @flow

import { createSelectorCreator } from 'reselect'
import { memoize } from 'lodash'
import deepEqual from 'deep-equal'

function myInefficientHashFunction(...args) {
    return JSON.stringify(args)
}

export const unboundedMemoizedSelectorCreator = createSelectorCreator(
    memoize,
    myInefficientHashFunction
)

const memo: { [key: string]: Array<any> } = {}
export function memoizeArray(array: Array<any>, key: string) {
    if (deepEqual(array, memo[key])) {
        return memo[key]
    }
    memo[key] = array
    return array
}
