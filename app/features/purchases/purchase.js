// @flow

import { _ } from 'lodash'
import { createSelector } from 'reselect'
import { memoizeArray } from '../../utils/memoize'

import { type RootState } from '../rootReducer'

// Types
export type Product = {|
    +productId: string,
    +title: string,
    +desctiption: string,
    +localizedPrice: string,
    +price: string,
|}

export type Purchase = {|
    +productId: string,
    +isPurchased: boolean,
    +questionPacks: Array<number>,
|}

export type PurchaseState = {|
    +areProductsFetching: boolean,
    +areProductsFetched: boolean,
    +fetchingProductId: string,
    +isRestoreFetching: boolean,
    +products: Array<Product>,
    +purchases: Array<Purchase>,
|}

export type setProductsFetchingAction = {|
    +type: 'SET_PRODUCTS_FETCHING',
    +payload: {|
        +areProductsFetching: boolean,
    |},
|}

export type setPurchaseFetchingAction = {|
    +type: 'SET_PURCHASE_FETCHING',
    +payload: {|
        +fetchingProductId: string,
    |},
|}

export type setRestoreFetchingAction = {|
    +type: 'SET_RESTORE_FETCHING',
    +payload: {|
        +isRestoreFetching: boolean,
    |},
|}

export type setProductsAction = {|
    +type: 'SET_PRODUCT_LIST',
    +payload: {|
        +products: Array<Product>,
    |},
|}

export type setPurchasesAction = {|
    +type: 'SET_PURCHASE_LIST',
    +payload: {|
        +purchases: Array<Purchase>,
    |},
|}

export type CommonAction =
    | setProductsFetchingAction
    | setPurchaseFetchingAction
    | setRestoreFetchingAction
    | setProductsAction
    | setPurchasesAction

// Reducer
const defaultState: PurchaseState = {
    areProductsFetching: false,
    areProductsFetched: false,
    fetchingProductId: '',
    isRestoreFetching: false,
    products: [],
    purchases: [],
}

export default function reducer(
    state: $Exact<PurchaseState> = defaultState,
    action: CommonAction
): $Exact<PurchaseState> {
    switch (action.type) {
        case 'SET_PRODUCTS_FETCHING': {
            const { areProductsFetching } = action.payload
            return {
                ...state,
                areProductsFetching,
            }
        }
        case 'SET_PURCHASE_FETCHING': {
            const { fetchingProductId } = action.payload
            return {
                ...state,
                fetchingProductId,
            }
        }
        case 'SET_RESTORE_FETCHING': {
            const { isRestoreFetching } = action.payload
            return {
                ...state,
                isRestoreFetching,
            }
        }
        case 'SET_PRODUCT_LIST': {
            const { products } = action.payload
            const areProductsFetching = false
            const areProductsFetched = true
            return {
                ...state,
                products: products,
                areProductsFetching,
                areProductsFetched,
            }
        }
        case 'SET_PURCHASE_LIST': {
            const { purchases } = action.payload
            const isRestoreFetching = false
            const fetchingProductId = ''
            return {
                ...state,
                purchases,
                isRestoreFetching,
                fetchingProductId,
            }
        }
        default: {
            return state
        }
    }
}

// Action creators
export const setProductsFetching = (): setProductsFetchingAction => {
    const type = 'SET_PRODUCTS_FETCHING'
    return { type, payload: { areProductsFetching: true } }
}

export const setPurchaseFetching = (
    fetchingProductId: string
): setPurchaseFetchingAction => {
    const type = 'SET_PURCHASE_FETCHING'
    return { type, payload: { fetchingProductId } }
}

export const setRestoreFetching = (): setRestoreFetchingAction => {
    const type = 'SET_RESTORE_FETCHING'
    return { type, payload: { isRestoreFetching: true } }
}

export const setProducts = (products: Array<Product>): setProductsAction => {
    const type = 'SET_PRODUCT_LIST'
    return { type, payload: { products } }
}

export const setPurchases = (
    purchases: Array<Purchase>
): setPurchasesAction => {
    const type = 'SET_PURCHASE_LIST'
    return { type, payload: { purchases } }
}

// Selectors
export const getAreProductsFetched = (state: RootState): boolean =>
    state.purchases.areProductsFetched

export const getAreProductsFetching = (state: RootState): boolean =>
    state.purchases.areProductsFetching

export const getIsRestoreFetching = (state: RootState): boolean =>
    state.purchases.isRestoreFetching

export const getProducts = (state: RootState): Array<Product> =>
    state.purchases.products

export const getPurchases = (state: RootState): Array<Purchase> =>
    state.purchases.purchases

export const getIsPurchased = (state: RootState, productId: string): boolean =>
    state.purchases.purchases.some(
        purchase => purchase.productId === productId && purchase.isPurchased
    )

export const getIsLargestPurchased = (state: RootState): boolean => {
    const largest = _.maxBy(state.purchases.products, product =>
        Number(product.price)
    )
    if (!largest) return false
    const isPurchased = state.purchases.purchases.some(
        purchase =>
            purchase.productId === largest.productId && purchase.isPurchased
    )
    return isPurchased
}

export const getIsPurchaseFetching = (
    state: RootState,
    productId: string
): boolean => state.purchases.fetchingProductId === productId

export const getIsSomePurchaseFetching = (state: RootState): boolean =>
    !_.isEmpty(state.purchases.fetchingProductId)

export const getPurchasedPacks = createSelector(
    (state: RootState) =>
        memoizeArray(state.purchases.purchases, 'PURCHASES_LIST'),
    (purchases: Array<Purchase>) =>
        purchases.reduce(
            (result, item) =>
                item.isPurchased
                    ? Array.from(new Set([...result, ...item.questionPacks]))
                    : result,
            []
        )
)

export const getProductIds = (state: RootState): Array<string> =>
    state.purchases.products.map(item => item.productId)
