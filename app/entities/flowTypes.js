// @flow

export type FlatListRenderItemInfo<ItemT> = {
    item: ItemT,
}

export type SectionListRenderItemInfo<ItemT> = {
    item: ItemT,
}

export type SectionListRenderHeaderInfo<SectionT> = {
    section: SectionT,
}
