// @flow

import {
    addNewSwotItemIntoStorage,
    deleteSwotItemFromStorage,
    updateSwotItemInStorage,
    getSwotItemListFromStorage,
    getQuestionListFromStorage,
    getSwotTitle,
} from '../../services/swotsStorage'

import {
    type UpdateSwotAction,
    type UpdateSwotItemListAction,
    getSwotId,
    updateSwot,
    updateSwotItemList,
} from './swot'

import htmlToPdf from '../../services/htmlToPdf'
import sharePdf from '../../services/sharePdf'

import { getAnswerChanges } from '../answers/answers'

import { localizedString } from '../../locales/localizedString'

import { type GetState } from '../rootReducer'

import {
    type SectionName,
    type SwotItem,
    type Question,
} from '../../entities/model'

type Action = UpdateSwotAction | UpdateSwotItemListAction
type Dispatch = (action: Action) => void
type ThunkAction = (dispatch: Dispatch, getState: GetState) => Promise<void>

async function updateCurrentSwotItemListAsync(
    dispatch: Dispatch,
    getState: GetState
) {
    const currentSwotId = getSwotId(getState())
    const updatedList: Array<SwotItem> = await getSwotItemListFromStorage(
        currentSwotId
    )
    dispatch(updateSwotItemList(updatedList))
}

const makeSection = (sectionName: SectionName, sourceList: Array<SwotItem>) => {
    const sectionItems = sourceList.filter(item => item.section === sectionName)
    return {
        name: sectionName,
        data: sectionItems,
    }
}

export function shareAsPdf(swotId: number, title: string): ThunkAction {
    return async () => {
        const list: Array<SwotItem> = await getSwotItemListFromStorage(swotId)
        const swotItems = [
            makeSection('Strengthes', list),
            makeSection('Weaknesses', list),
            makeSection('Opportunities', list),
            makeSection('Threats', list),
        ]
        let html = `<h1>${title}</h1>\n`
        swotItems.forEach(section => {
            html += `<h3>${section.name}</h3>\n`
            html += '<ul>\n'
            const answers = section.data
            answers.forEach(answer => {
                const clearedText = answer.text.replace(
                    /<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g,
                    ''
                )
                html += `<li>${clearedText}</li>\n`
            })
            html += '</ul>\n'
        })

        const results = await htmlToPdf.convert({
            html,
            fileName: localizedString('swotScreen.pdfTitle'),
            directory: 'cache',
            padding: 30,
        })
        sharePdf.share(results.filePath)
        // dispatch(updateSwot(swotId, title, list, questions))
    }
}

export function prepareSwot(swotId: number): ThunkAction {
    return async (dispatch: Dispatch) => {
        const title: string = (await getSwotTitle(swotId)) || 'no'
        const list: Array<SwotItem> = await getSwotItemListFromStorage(swotId)
        // TODO_FUTURE: пакет не должен быть задан константой (извлекать из базы)
        const packIDs = [1, 2, 3]
        const questions: Array<Question> = await getQuestionListFromStorage(
            packIDs
        )
        dispatch(updateSwot(swotId, title, list, questions))
    }
}

export function deleteSwotItem(swotItemId: number): ThunkAction {
    return async (dispatch: Dispatch, getState: GetState) => {
        await deleteSwotItemFromStorage(swotItemId)
        await updateCurrentSwotItemListAsync(dispatch, getState)
    }
}

export function applyAnswerChangesAsync(): ThunkAction {
    return async (dispatch: Dispatch, getState: GetState) => {
        const answerChanges = getAnswerChanges(getState())
        const { brandNewChanges, editedChanges } = answerChanges

        const asyncs = []
        editedChanges.forEach(change => {
            const { swotItemId, text } = change
            asyncs.push(updateSwotItemInStorage(swotItemId, text))
        })
        brandNewChanges.forEach(change => {
            const { swotId, section, text, questionId } = change
            asyncs.push(
                addNewSwotItemIntoStorage(swotId, section, text, questionId)
            )
        })
        await Promise.all(asyncs)

        await updateCurrentSwotItemListAsync(dispatch, getState)
    }
}
