import json
import os
import sys

def findAndUpdate(dictionary, searchPath):
    hasArray = False
    for k in dictionary['objects']:
        node = dictionary['objects'][k]
        for kk in node:
            if kk == 'buildSettings':
                buildSettings = node[kk]
                for kkk in buildSettings:
                    searchPaths = buildSettings[kkk]
                    if kkk == 'HEADER_SEARCH_PATHS' and isinstance(searchPaths, list):
                        hasArray = True
                        searchPaths.append(searchPath)
    if hasArray == False:
        for k in dictionary['objects']:
            node = dictionary['objects'][k]
            for kk in node:
                if kk == 'buildSettings':
                    buildSettings = node[kk]
                    buildSettings['HEADER_SEARCH_PATHS'] = searchPath

def addSearchPathHeaderToXCodeProj(xCodeProj, searchPath):

    pbxFilePath = xCodeProj + "/project.pbxproj"
    jsonFilePath = xCodeProj + "/Data.json"

    # convert pbxproj format to json
    convertToJsonCommand = "plutil -convert json -o {0} {1}".format(jsonFilePath, pbxFilePath)
    os.system(convertToJsonCommand)

    # insert item into buildSettings->HEADER_SEARH_PATH section
    with open(jsonFilePath, 'r') as f:
        data = json.load(f)

    findAndUpdate(data, searchPath)

    os.remove(jsonFilePath)
    with open(jsonFilePath, 'w') as f:
        json.dump(data, f, indent=4)

    # xCode can read xml1 format; the moment you change any value in Xcode, it rewrites the file using the pbxproj format
    convertToXml1Command = "plutil -convert xml1 {0} -o {1}".format(jsonFilePath, pbxFilePath)
    os.system(convertToXml1Command)
    os.remove(jsonFilePath)
