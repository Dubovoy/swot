// @flow

import { _ } from 'lodash'
import { createSelector } from 'reselect'
import { memoizeArray } from '../../utils/memoize'

import {
    type SectionName,
    type SwotItem,
    type Question,
} from '../../entities/model'

import { type SwotSection } from '../../components/swotSection/SwotSection'

import { type RootState } from '../rootReducer'

// Types
export type SwotState = {|
    +id: number,
    +title: string,
    +swotItemList: Array<SwotItem>,
    +minimizedSections: Array<SectionName>,
    +questions: Array<Question>,
|}

export type UpdateSwotItemListAction = {
    +type: 'UPDATE_SWOT_ITEM_LIST',
    +payload: { +list: Array<SwotItem> },
}

export type UpdateSwotAction = {|
    +type: 'UPDATE_SWOT',
    +payload: {|
        +id: number,
        +title: string,
        +list: Array<SwotItem>,
        +questions: Array<Question>,
    |},
|}

export type SwitchSectionCollapseStateAction = {|
    +type: 'SWITCH_SECTION_COLLAPSE_STATE',
    +payload: {| +sectionName: SectionName |},
|}

// Reducer
const defaultState: SwotState = {
    id: -1,
    title: '',
    swotItemList: [],
    minimizedSections: ['Weaknesses'],
    questions: [],
}

export default function reducer(
    state: $Exact<SwotState> = defaultState,
    action:
        | UpdateSwotAction
        | UpdateSwotItemListAction
        | SwitchSectionCollapseStateAction
): $Exact<SwotState> {
    switch (action.type) {
        case 'UPDATE_SWOT': {
            const { id, title, list, questions } = action.payload
            return { ...state, id, title, swotItemList: list, questions }
        }
        case 'UPDATE_SWOT_ITEM_LIST': {
            const { list } = action.payload
            return { ...state, swotItemList: list }
        }
        case 'SWITCH_SECTION_COLLAPSE_STATE': {
            const { sectionName } = action.payload
            const { minimizedSections } = state
            const updatedSections = minimizedSections.includes(sectionName)
                ? minimizedSections.filter(value => value !== sectionName)
                : minimizedSections.concat(sectionName)
            return {
                ...state,
                minimizedSections: updatedSections,
            }
        }
        case 'EXPAND_SECTION': {
            const { sectionName } = action.payload
            const { minimizedSections } = state
            const updatedSections = minimizedSections.filter(
                value => value !== sectionName
            )
            return {
                ...state,
                minimizedSections: updatedSections,
            }
        }
        default: {
            return state
        }
    }
}

// Action creators
export const updateSwotItemList = (
    list: Array<SwotItem>
): UpdateSwotItemListAction => {
    const type = 'UPDATE_SWOT_ITEM_LIST'
    return { type, payload: { list } }
}

export const updateSwot = (
    id: number,
    title: string,
    list: Array<SwotItem>,
    questions: Array<Question>
): UpdateSwotAction => {
    const type = 'UPDATE_SWOT'
    return { type, payload: { id, title, list, questions } }
}

export const switchSectionCollapseState = (
    sectionName: SectionName
): SwitchSectionCollapseStateAction => {
    const type = 'SWITCH_SECTION_COLLAPSE_STATE'
    return { type, payload: { sectionName } }
}

// Selectors
export const getSwotId = (state: RootState): number => state.currentSwot.id

export const getSwotTitle = (state: RootState): string =>
    state.currentSwot.title

const makeSection = (
    sectionName: SectionName,
    sourceList: Array<SwotItem>,
    minimizedSections: Array<SectionName>,
    questions: Array<Question>
) => {
    const sectionItems = sourceList.filter(item => item.section === sectionName)
    const sectionQuestions = questions.filter(
        question => question.section === sectionName
    )
    const orderedSectionItems = sectionQuestions.reduce((result, question) => {
        const swotItem = sectionItems.find(
            item => item.questionId === question.id
        )
        if (swotItem) {
            result.push(swotItem)
        }
        return result
    }, [])
    const isMinimized = minimizedSections.includes(sectionName)
    return {
        name: sectionName,
        data: isMinimized ? [] : orderedSectionItems,
        isMinimized,
        isEmpty: _.isEmpty(sectionItems),
    }
}

export const getSwotItemList = createSelector(
    (state: RootState) =>
        memoizeArray(state.currentSwot.swotItemList, 'SWOT_ITEM_LIST'),
    (state: RootState) => state.currentSwot.minimizedSections,
    (state: RootState) => state.currentSwot.questions,
    (
        swotItemList: Array<SwotItem>,
        minimizedSections: Array<SectionName>,
        questions: Array<Question>
    ): Array<SwotSection> => [
        makeSection('Strengthes', swotItemList, minimizedSections, questions),
        makeSection('Weaknesses', swotItemList, minimizedSections, questions),
        makeSection(
            'Opportunities',
            swotItemList,
            minimizedSections,
            questions
        ),
        makeSection('Threats', swotItemList, minimizedSections, questions),
    ]
)
