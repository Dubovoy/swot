// @flow strict

import { type SwotItem, type Question } from './model'

export class CarouselCard {}

export class QuestionCard extends CarouselCard {
    question: Question

    swotItem: ?SwotItem

    constructor(question: Question, swotItem: ?SwotItem) {
        super()
        this.question = question
        this.swotItem = swotItem
    }
}

export class MoreQuestionsCard extends CarouselCard {}
