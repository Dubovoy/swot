// @flow strict

const typography = {
    fontWeight: {
        header: 'bold',
        textBlock: 'normal',
        itemTitle: 'bold',
        itemSubtitle: 'normal',
    },
    fontSize: {
        header: 18,
        textBlock: 16,
        itemTitle: 18,
        itemSubtitle: 14,
    },
}

export default typography
