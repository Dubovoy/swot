// @flow

import {
    getSwotItemListFromStorage,
    getQuestionListFromStorage,
} from '../../services/swotsStorage'

import {
    type setSwotInfoAction,
    type SetCardsAction,
    setSwotInfo,
    setCards,
    getSectionName,
    getSwotId,
} from './answers'

import { getPurchasedPacks } from '../purchases/purchase'

import { type GetState } from '../rootReducer'

import {
    type SwotItem,
    type SectionName,
    type Question,
} from '../../entities/model'

import { QuestionCard } from '../../entities/viewModel'

type Action = setSwotInfoAction | SetCardsAction
type Dispatch = (action: Action) => void
type ThunkAction = (dispatch: Dispatch, getState: GetState) => Promise<void>

async function makeCards(
    swotId: number,
    section: SectionName,
    packIDs: Array<number>
): Promise<Array<QuestionCard>> {
    console.log(packIDs)
    const questions: Array<Question> = await getQuestionListFromStorage(
        packIDs,
        section
    )
    const answers: Array<SwotItem> = await getSwotItemListFromStorage(
        swotId,
        section
    )

    return questions.map(
        question =>
            new QuestionCard(
                question,
                answers.find(item => item.questionId === question.id)
            )
    )
}

export function setSwotInfoAsync(
    swotId: number,
    section: SectionName,
    selectedSwotItemId: ?number
): ThunkAction {
    return async (dispatch: Dispatch, getState: GetState) => {
        const packIds = getPurchasedPacks(getState())
        const cards = await makeCards(swotId, section, packIds)
        const firstCardIndex = cards.findIndex(card => {
            if (selectedSwotItemId) {
                return card.swotItem && card.swotItem.id === selectedSwotItemId
            }
            return card.swotItem === undefined
        })
        dispatch(setSwotInfo(swotId, section, firstCardIndex))
        dispatch(setCards(cards))
    }
}

export function updateCardsAsync(): ThunkAction {
    return async (dispatch: Dispatch, getState: GetState) => {
        const state = getState()
        const swotId = getSwotId(state)
        const section = getSectionName(state)
        const packIds = getPurchasedPacks(state)
        const cards = await makeCards(swotId, section, packIds)
        dispatch(setCards(cards))
    }
}
