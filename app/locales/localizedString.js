// @flow

import i18next from 'i18next'
import locale from 'react-native-locale-detector'

import en from './en.json'
// import ru from './ru.json'

import { type TFunction } from './en.translation.js.flow'

const languagesMap = {
    en,
    // ru,
}

const systemLanguageCode = locale.substring(0, 2)
const systemLanguageData = languagesMap[systemLanguageCode]
const languageCode = systemLanguageData ? systemLanguageCode : 'en'
const languageData = systemLanguageData || en

i18next.init(
    {
        lng: languageCode,
        debug: __DEV__,
        resources: languageData,
    },
    (err, t) => {
        console.log(err)
        console.log(t)
    }
)

export const localizedString: TFunction = (name: string) => i18next.t(name)
