// @flow

import { Alert, NetInfo } from 'react-native'

// Locales
import { localizedString } from '../locales/localizedString'

export const baseUrl = 'https://apps.dubovoy.pro'
export const inappEndpoint = '/inapp_personal_swot'
export const productsEndpoint = '/inapp_personal_swot/products'

export const methods = { post: 'post', get: 'get' }
export const ErrorCodes = {
    NoConnection: 'NO_CONEECTION',
    ServiceUnavailable: 'SERVICE_UNAVAILABLE',
}

const isDisconnected = async () => {
    const connection = await NetInfo.getConnectionInfo()
    return !(connection.type === 'wifi' || connection.type === 'cellular')
}

const showNoConnectionAlert = () => {
    return setTimeout(() => {
        Alert.alert(
            localizedString('alerts.noConnection.title'),
            localizedString('alerts.noConnection.message')
        )
    }, 300) // set timeout to avoid lock of react/redux pipeline
}

const showServiceUnavailableAlert = () => {
    return setTimeout(() => {
        Alert.alert(
            localizedString('alerts.serviceUnavailable.title'),
            localizedString('alerts.serviceUnavailable.message')
        )
    }, 300) // set timeout to avoid lock of react/redux pipeline
}

const isJsonType = response =>
    response &&
    response.headers &&
    response.headers.get('content-type') &&
    response.headers.get('content-type').includes('application/json')

const encodeBody = obj => {
    const arr = Object.keys(obj).reduce((acc, key) => {
        const objValue = obj[key]
        if (objValue) {
            acc.push(`${key}=${encodeURIComponent(objValue)}`)
        }
        return acc
    }, [])
    return arr.join('&')
}

export const getProductsAsync = async () => {
    const requestUrl = `${baseUrl}${productsEndpoint}`
    const headers: Headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
    })
    if (await isDisconnected()) {
        showNoConnectionAlert()
        throw new Error(ErrorCodes.NoConnection)
    }
    const response = await fetch(requestUrl, {
        method: methods.get,
        headers,
    })
    if (!isJsonType(response)) {
        showServiceUnavailableAlert()
        throw new Error(ErrorCodes.ServiceUnavailable)
    }
    const json = await response.json()
    return json.productIds // TODO: типизировать ответ!
}

// TODO: по REST-семантике тип запроса должен быть GET, а не POST
export const getValidationStatus = async (transaction: string) => {
    const requestUrl = `${baseUrl}${inappEndpoint}`
    const headers: Headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
    })
    if (await isDisconnected()) {
        showNoConnectionAlert()
        throw new Error(ErrorCodes.NoConnection)
    }
    const response = await fetch(requestUrl, {
        method: methods.post,
        headers,
        body: encodeBody({ transaction_receipt: transaction }),
    })
    if (!isJsonType(response)) {
        showServiceUnavailableAlert()
        throw new Error(ErrorCodes.ServiceUnavailable)
    }
    const purchases = await response.json()
    return purchases
}
