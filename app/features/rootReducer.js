// @flow

import { combineReducers } from 'redux'

import swotList, { type SwotListState } from './swotList/swotList'
import currentSwot, { type SwotState } from './currentSwot/swot'
import answers, { type AnswersState } from './answers/answers'
import purchases, { type PurchaseState } from './purchases/purchase'
import onboarding, { type OnboardingState } from './onboarding/onboarding'

const rootReducer = combineReducers({
    onboarding,
    swotList,
    currentSwot,
    answers,
    purchases,
})

export type RootState = {|
    +onboarding: OnboardingState,
    +swotList: SwotListState,
    +currentSwot: SwotState,
    +answers: AnswersState,
    +purchases: PurchaseState,
|}

export type GetState = () => RootState

export default rootReducer
