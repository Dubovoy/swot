// @flow

// Library tools
import * as React from 'react'

// Library components
import { Text, View } from 'react-native'
import Swipeout from 'react-native-swipeout'

// App components
import withTouchHandlers from '../HOCs/withTouchHandlers'

// Styles
import layouts from '../../styles/layouts'
import palette from '../../styles/palette'

// Locales
import { localizedString } from '../../locales/localizedString'

// Entities
import { type SwotItem } from '../../entities/model'
import { type SectionListRenderItemInfo } from '../../entities/flowTypes'

type OnSwotItemJesture = (swotItem: SwotItem) => void

export function makeSwotItemView(
    onOpenSwotItem: OnSwotItemJesture,
    onDeleteSwotItem: OnSwotItemJesture
) {
    return (info: SectionListRenderItemInfo<SwotItem>): React.Element<any> => {
        const { item } = info
        const { text } = item
        const ItemShape = () => (
            <View style={layouts.listItem.container}>
                <Text style={layouts.listItem.text}>{`${text}`}</Text>
            </View>
        )
        const SwotItemView = withTouchHandlers(
            { senderKey: item, onPress: onOpenSwotItem },
            { senderKey: item, onLongPress: onDeleteSwotItem }
        )(ItemShape)

        const swipeoutBtns = [
            {
                text: localizedString('components.swipeButton.title'),
                backgroundColor: palette.deleteRed,
                onPress: () => {
                    onDeleteSwotItem(item)
                },
            },
        ]

        return (
            <Swipeout right={swipeoutBtns}>
                <SwotItemView />
            </Swipeout>
        )
    }
}
