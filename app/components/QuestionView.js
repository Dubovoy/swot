// @flow

// Library tools
import * as React from 'react'
import {
    Dimensions,
    Keyboard,
    Text,
    TextInput,
    SafeAreaView,
    ScrollView,
} from 'react-native'

// Styles
import layouts from '../styles/layouts'

type Props = {
    questionId: number,
    questionText: string,
    answerText: string,
    editedAnswerText: string,
    editAnswer: (questionId: number, text: string) => void,
}

type State = {
    keyBoardHeight: number,
}

class QuestionView extends React.Component<Props, State> {
    keyboardWillShowListener = null

    keyboardWillHideListener = null

    constructor(props: Props) {
        super(props)
        this.state = { keyBoardHeight: 0 }
    }

    componentDidMount() {
        this.keyboardWillShowListener = Keyboard.addListener(
            'keyboardWillShow',
            this.keyboardWillShow
        )
        this.keyboardWillHideListener = Keyboard.addListener(
            'keyboardWillHide',
            this.keyboardWillHide
        )
    }

    componentWillUnmount() {
        this.keyboardWillShowListener && this.keyboardWillShowListener.remove()
        this.keyboardWillHideListener && this.keyboardWillHideListener.remove()
    }

    keyboardWillShow = e => {
        const { width, height } = Dimensions.get('window')
        const portraitTabBarOffset = width < height ? 44 : 0
        this.setState({
            keyBoardHeight: e.endCoordinates.height - portraitTabBarOffset,
        })
    }

    keyboardWillHide = e => {
        this.setState({
            keyBoardHeight: 0,
        })
    }

    render(): React.Element<any> {
        const {
            questionId,
            questionText,
            answerText,
            editedAnswerText,
            editAnswer,
        } = this.props
        const { keyBoardHeight } = this.state

        const editCurrentAnswer = text => {
            editAnswer(questionId, text)
        }
        const textValue =
            editedAnswerText === ''
                ? editedAnswerText
                : editedAnswerText || answerText
        const scrollViewStyle = {
            ...layouts.containers.centeredContainer,
            paddingBottom: keyBoardHeight,
        }

        return (
            <SafeAreaView style={layouts.containers.stretchedContainer}>
                <ScrollView contentContainerStyle={scrollViewStyle}>
                    <Text
                        style={layouts.basicText.questionContainer}
                    >{`${questionText}`}</Text>
                    <TextInput
                        style={layouts.basicText.answerField}
                        multiline
                        value={textValue}
                        onChangeText={editCurrentAnswer}
                        placeholder="Type answer"
                    />
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default QuestionView
