// @flow

import * as RNIap from 'react-native-iap'
import { _ } from 'lodash'

import { getProductsAsync, getValidationStatus } from '../../services/rest'

import {
    type CommonAction,
    setProductsFetching,
    setPurchaseFetching,
    setRestoreFetching,
    setProducts,
    setPurchases,
    getProductIds,
} from './purchase'

import { type GetState } from '../rootReducer'

type Dispatch = (action: CommonAction) => void
type ThunkAction = (dispatch: Dispatch, getState: GetState) => Promise<void>

export function updateProductsAsync(): ThunkAction {
    return async (dispatch: Dispatch) => {
        try {
            dispatch(setProductsFetching())
            // get product descriptions from own server
            const productDescriptions = await getProductsAsync()

            if (!productDescriptions || _.isEmpty(productDescriptions)) {
                throw new Error('No product descriptions')
            }
            const productIds = productDescriptions.map(
                product => product.productId
            )

            // get product list from Apple server
            const productsSrc = await RNIap.getProducts(productIds)
            if (!productsSrc || _.isEmpty(productsSrc)) {
                throw new Error('No products list')
            }
            const products = productsSrc.map(item => {
                const {
                    productId,
                    title,
                    description,
                    price,
                    localizedPrice,
                } = item
                return {
                    productId,
                    title,
                    description,
                    price,
                    localizedPrice,
                }
            })

            // order the product list in accordance with descriptions from own server
            const getOrderIndex = (productId: string) => {
                const product = productDescriptions.find(
                    item => item.productId === productId
                )
                return product && product.orderIndex
            }
            const sortedProducts = products.sort(
                (a, b) =>
                    getOrderIndex(a.productId) - getOrderIndex(b.productId)
            )
            dispatch(setProducts(sortedProducts))
        } catch (ex) {
            console.log(ex)
            dispatch(setProducts([]))
        }
    }
}

export function makePurchaseAsync(productId: string): ThunkAction {
    return async (dispatch: Dispatch, getState: GetState) => {
        try {
            dispatch(setPurchaseFetching(productId))
            const productIds = getProductIds(getState())
            const products = await RNIap.getProducts(productIds)
            if (!products || _.isEmpty(products)) {
                throw new Error('No products list')
            }
            const purchase = await RNIap.buyProduct(productId)
            if (!purchase) {
                throw new Error('No purchase')
            }
            if (!purchase.transactionReceipt) {
                throw new Error('No transactionReceipt')
            }
            const purchases = await getValidationStatus(
                purchase.transactionReceipt
            )
            dispatch(setPurchases(purchases))
        } catch (ex) {
            console.log(ex)
            dispatch(setPurchaseFetching(''))
        }
    }
}

export function restoreAllPurchasesAsync(): ThunkAction {
    return async (dispatch: Dispatch) => {
        try {
            dispatch(setRestoreFetching())
            const purchasedProducts = await RNIap.getAvailablePurchases()
            if (!purchasedProducts || _.isEmpty(purchasedProducts)) {
                return
            }
            const receipt = purchasedProducts[0].transactionReceipt
            if (!receipt) {
                throw new Error('No receipt')
            }
            const purchases = await getValidationStatus(receipt)
            dispatch(setPurchases(purchases))
        } catch (ex) {
            console.log(ex)
            dispatch(setPurchases([]))
        }
    }
}
