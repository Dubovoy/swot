// @flow

import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import ReduxThunk from 'redux-thunk'
import storage from 'redux-persist/lib/storage'

import rootReducer from './features/rootReducer'

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const devMiddlewares = []
if (__DEV__) {
    // eslint-disable-next-line no-underscore-dangle
    const reduxTools = global.__REDUX_DEVTOOLS_EXTENSION__
    if (reduxTools && typeof reduxTools === typeof Function) {
        devMiddlewares.push(reduxTools())
    }
}

const createStoreWithMiddleware = compose(
    applyMiddleware(ReduxThunk),
    ...devMiddlewares
)(createStore)

export const store = createStoreWithMiddleware(persistedReducer)
export const persistor = persistStore(store)
