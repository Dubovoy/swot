// @flow

// Library tools
import * as React from 'react'
import { connect } from 'react-redux'

// Library components
import {
    Button,
    Dimensions,
    Image,
    View,
    SafeAreaView,
    ScrollView,
    Text,
} from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'

// App components
import PurchaseList from '../purchases/PurchaseList'

// Business logic
import { setWasShown } from './onboarding'
import { updateProductsAsync } from '../purchases/purchaseAsyncActions'

// Styles
import layouts from '../../styles/layouts'
import palette from '../../styles/palette'

// Locales
import { localizedString } from '../../locales/localizedString'

// Utils
import { reportEvent } from '../../services/metrica'

// Entities
type StateMapProps = {}

type DispatchMapProps = {|
    +close: () => void,
    +updateProducts: () => void,
|}
type State = {|
    width: number,
    pageIndex: number,
|}

type Props = StateMapProps & DispatchMapProps

// Resources
const onboardingImg1 = require('../../images/onboarding_swot.png')
const onboardingImg2 = require('../../images/onboarding_zen_stones.png')

const pages = [
    {
        backgroundColor: '#fff',
        image: (
            <Image
                source={onboardingImg1}
                style={layouts.containers.onboardingImage}
            />
        ),
        title: localizedString('onboardingScreen.page1.title'),
        subtitle: localizedString('onboardingScreen.page1.subtitle'),
    },
    {
        backgroundColor: '#fff',
        image: (
            <Image
                source={onboardingImg2}
                style={layouts.containers.onboardingImage}
            />
        ),
        title: localizedString('onboardingScreen.page2.title'),
        subtitle: localizedString('onboardingScreen.page2.subtitle'),
    },
    {
        backgroundColor: '#fff',
        image: <Image />,
        title: localizedString('onboardingScreen.page3.title'),
        subtitle: (
            <View style={layouts.containers.stretchedContainer}>
                <PurchaseList env="onboardingScreen" />
            </View>
        ),
    },
]

class OnboardingScreen extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        const { width } = Dimensions.get('window')
        const pageIndex = 0
        this.state = { width, pageIndex }
    }

    componentDidMount() {
        reportEvent({ onboarding: { viewShown: 'onboardingScreen' } })
    }

    onLayout = () => {
        const { width } = Dimensions.get('window')
        this.setState({ width })
    }

    onBeforeSnapToItem = pageIndex => {
        reportEvent({ onboarding: { swipe: `$toPage${pageIndex}` } })
        const { updateProducts } = this.props
        this.setState({ pageIndex })
        if (pageIndex === pages.length - 1) {
            updateProducts()
        }
    }

    get skipButton() {
        const { close } = this.props
        const skip = () => {
            reportEvent({ onboarding: { buttonPressed: 'skip' } })
            close()
        }
        return (
            <View style={layouts.containers.sideButtonContainer}>
                <Button
                    title={localizedString('onboardingScreen.buttons.skip')}
                    onPress={skip}
                    color={palette.navbar}
                />
            </View>
        )
    }

    get nextButton() {
        const next = () => {
            reportEvent({ onboarding: { buttonPressed: 'next' } })
            this.carouselRef.snapToNext()
        }
        return (
            <View style={layouts.containers.sideButtonContainer}>
                <Button
                    title={localizedString('onboardingScreen.buttons.next')}
                    onPress={next}
                    color={palette.navbar}
                />
            </View>
        )
    }

    get doneButton() {
        const { close } = this.props
        const done = () => {
            reportEvent({ onboarding: { buttonPressed: 'done' } })
            close()
        }
        return (
            <View style={layouts.containers.sideButtonContainer}>
                <Button
                    title={localizedString('onboardingScreen.buttons.done')}
                    onPress={done}
                    color={palette.navbar}
                />
            </View>
        )
    }

    get pagination() {
        const { pageIndex } = this.state
        const emptyButton = <View style={layouts.containers.sideButtonContainer} />
        const leftButton = this.isLastPageIndex(pageIndex)
            ? emptyButton
            : this.skipButton
        const rightButton = this.isLastPageIndex(pageIndex)
            ? this.doneButton
            : this.nextButton
        return (
            <View style={layouts.containers.paginationContainer}>
                {leftButton}
                <Pagination
                    dotsLength={pages.length}
                    activeDotIndex={pageIndex}
                />
                {rightButton}
            </View>
        )
    }

    isLastPage = page => {
        const len = pages.length
        if (len === 0) return false
        return page === pages[pages.length - 1]
    }

    isLastPageIndex = pageIndex => {
        const len = pages.length
        if (len === 0) return false
        return pageIndex === pages.length - 1
    }

    carouselRef: Carousel

    renderItem = ({ item }) => {
        const { subtitle } = item
        let subtitleView
        if (typeof subtitle === 'string' || subtitle instanceof String) {
            subtitleView = (
                <Text style={layouts.basicText.regular}>{`${
                    item.subtitle
                }`}</Text>
            )
        } else {
            subtitleView = subtitle
        }
        const page = (
            <View style={layouts.containers.centeredContainer}>
                {item.image}
                <Text style={layouts.basicText.header}>{item.title}</Text>
                {subtitleView}
            </View>
        )

        return (
            <View style={layouts.containers.allAvailableSpace}>
                {this.isLastPage(item) ? page : <ScrollView>{page}</ScrollView>}
            </View>
        )
    }

    render(): React.Element<any> {
        const { width } = this.state
        return (
            <SafeAreaView
                onLayout={this.onLayout}
                style={layouts.containers.allAvailableSpace}
            >
                <Carousel
                    ref={c => {
                        this.carouselRef = c
                    }}
                    data={pages}
                    renderItem={this.renderItem}
                    firstItem={0}
                    sliderWidth={width}
                    itemWidth={width}
                    onBeforeSnapToItem={this.onBeforeSnapToItem}
                />
                {this.pagination}
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (): StateMapProps => ({})

const mapDispatchToProps = (dispatch): DispatchMapProps => ({
    close: () => dispatch(setWasShown(true)),
    updateProducts: () => dispatch(updateProductsAsync()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OnboardingScreen)
