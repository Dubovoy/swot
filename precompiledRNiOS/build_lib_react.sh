# define folder environment variables
CONFIGURATION=$1
LIB_NAME=$2
TARGET_NAME=$3
XCODEPROJ_PATH=$4
echo LIB_NAME $LIB_NAME
echo TARGET_NAME $TARGET_NAME
echo $XCODEPROJ_PATH

LIB_OUTPUT=$5
HEADERS_OUTPUT=$6

SIMULATOR_OUTPUTFOLDER="${PWD}/libs_${CONFIGURATION}/${LIB_NAME}/iphonesimulator"
DEVICE_OUTPUTFOLDER="${PWD}/libs_${CONFIGURATION}/${LIB_NAME}/iphoneos"
UNIVERSAL_OUTPUTFOLDER="${PWD}/libs_${CONFIGURATION}/${LIB_NAME}/universal"

# build lib for simulator
xcodebuild -quiet -project "${XCODEPROJ_PATH}" \
			ONLY_ACTIVE_ARCH=NO \
            BITCODE_GENERATION_MODE=bitcode \
		  -target "${TARGET_NAME}" \
		  -configuration $CONFIGURATION \
		  -sdk iphonesimulator \
		  CONFIGURATION_BUILD_DIR="${SIMULATOR_OUTPUTFOLDER}"

# build twice to workaround and issue:
if [[ $TARGET_NAME = React ]]; then
xcodebuild -quiet -project "${XCODEPROJ_PATH}" \
			ONLY_ACTIVE_ARCH=NO \
            BITCODE_GENERATION_MODE=bitcode \
		  -target "${TARGET_NAME}" \
		  -configuration $CONFIGURATION \
		  -sdk iphonesimulator \
		  CONFIGURATION_BUILD_DIR="${SIMULATOR_OUTPUTFOLDER}"
fi

# build lib for device
xcodebuild -quiet -project "${XCODEPROJ_PATH}" \
			ONLY_ACTIVE_ARCH=NO \
			BITCODE_GENERATION_MODE=bitcode \
		  -target "${TARGET_NAME}" \
		  -configuration $CONFIGURATION \
		  -sdk iphoneos \
		  CONFIGURATION_BUILD_DIR="${DEVICE_OUTPUTFOLDER}"

# merge libs into universal one
mkdir -p "${UNIVERSAL_OUTPUTFOLDER}"
lipo -create -output "${LIB_OUTPUT}/${LIB_NAME}.a" "${SIMULATOR_OUTPUTFOLDER}/${LIB_NAME}.a" "${DEVICE_OUTPUTFOLDER}/${LIB_NAME}.a"
echo "****** BUILD FINISHED ******"
echo ${UNIVERSAL_OUTPUTFOLDER}/${LIB_NAME}.a

# copy headers for universal lib
cp -R "${DEVICE_OUTPUTFOLDER}/include/" "${UNIVERSAL_OUTPUTFOLDER}/include"
cp -R "${DEVICE_OUTPUTFOLDER}/include/" "${HEADERS_OUTPUT}/include"
