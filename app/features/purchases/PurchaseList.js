// @flow

// Library tools
import * as React from 'react'
import { connect } from 'react-redux'
import { _ } from 'lodash'

// Library components
import { FlatList, View, RefreshControl, ScrollView, Text } from 'react-native'
import { WaveIndicator } from 'react-native-indicators'

// App components
import withTouchHandlers from '../../components/HOCs/withTouchHandlers'
import FlatListFooterStub from '../../components/FlatListFooterStub'

// Styles
import layouts from '../../styles/layouts'

// Business logic
import {
    type Product,
    getAreProductsFetching,
    getProducts,
    getAreProductsFetched,
    getIsPurchased,
    getIsPurchaseFetching,
    getIsSomePurchaseFetching,
} from './purchase'
import { makePurchaseAsync, updateProductsAsync } from './purchaseAsyncActions'

// Utils
import { reportEvent } from '../../services/metrica'

// Entities
import { type FlatListRenderItemInfo } from '../../entities/flowTypes'
import { type RootState } from '../rootReducer'
import palette from '../../styles/palette'
import { localizedString } from '../../locales/localizedString'

type OwnProps = {|
    +env: string,
|}

type StateMapProps = {|
    +products: Array<Product>,
    +areProductsFetching: boolean,
    +areProductsFetched: boolean,
|}

type DispatchMapProps = {|
    +buy: (productId: string) => void,
    +update: () => void,
|}

type Props = StateMapProps & DispatchMapProps & OwnProps

class PurchaseList extends React.Component<Props> {
    // TODO: connect-ы нужно уносить внутрь компомнента, и типизировать
    // точно так же, как типизируем connect для экрана, с Union
    // различных типов
    connectToPurchases = connect((state, ownProps) => ({
        ...ownProps,
        isPurchased: getIsPurchased(state, ownProps.productId),
        isPurchasing: getIsPurchaseFetching(state, ownProps.productId),
        isOtherPurchasing: getIsSomePurchaseFetching(state),
    }))

    renderItem = (
        info: FlatListRenderItemInfo<Product>
    ): React.Element<any> => {
        const { item } = info
        const { buy, env } = this.props
        const buyWithEvent = (productId: string) => {
            reportEvent({
                purchaseList: {
                    buyButtonPressed: { [productId]: env },
                },
            })
            buy(productId)
        }

        const pressHandler = {
            senderKey: item.productId,
            onPress: buyWithEvent,
        }
        const BuyButtonShape = ({
            isPurchased,
            isPurchasing,
            isOtherPurchasing,
        }) => {
            const isFree = item.price === '0'
            const buyButtonText = isFree
                ? localizedString('purchaseList.getForFree')
                : localizedString('purchaseList.buy')
            const buyButtonStyle = isFree
                ? layouts.button.buyButtonFree
                : layouts.button.buyButton
            const BuyButtonContent = () => (
                <View style={buyButtonStyle}>
                    {isPurchasing ? (
                        <WaveIndicator color={palette.contrastLightText} />
                    ) : (
                        <Text
                            style={layouts.basicText.lightText}
                        >{`${buyButtonText}`}</Text>
                    )}
                </View>
            )
            const AvailableContainer = () => (
                <View style={layouts.button.availablePanel}>
                    <Text style={layouts.basicText.greenText}>
                        {localizedString('purchaseList.downloaded')}
                    </Text>
                </View>
            )
            const buyButton = isOtherPurchasing
                ? BuyButtonContent
                : withTouchHandlers(pressHandler)(BuyButtonContent)
            const InteractiveButton = isPurchased
                ? AvailableContainer
                : buyButton
            return <InteractiveButton />
        }
        const BuyButton = this.connectToPurchases(BuyButtonShape)

        const PurchaseViewShape = () => (
            <View style={layouts.listItem.container}>
                <View style={layouts.listItem.container}>
                    <Text style={layouts.listItem.title}>{`${
                        item.title
                    }`}</Text>
                    <Text style={layouts.listItem.subtitle}>{`${
                        item.localizedPrice
                    }`}</Text>
                </View>
                <BuyButton productId={item.productId} />
            </View>
        )
        return <PurchaseViewShape />
    }

    separator = (): React.Element<any> => (
        <View style={layouts.listItem.separator} />
    )

    keyExtractor = (item: Product): string => String(item.productId)

    render(): React.Element<any> {
        const {
            products,
            areProductsFetching,
            update,
            areProductsFetched,
        } = this.props
        const productsAvailable = !_.isEmpty(products)
        const isInitialRequest = !areProductsFetched
        let contentView
        if (productsAvailable || isInitialRequest) {
            contentView = (
                <FlatList
                    renderItem={this.renderItem}
                    ItemSeparatorComponent={this.separator}
                    data={products}
                    keyExtractor={this.keyExtractor}
                    refreshing={areProductsFetching}
                    onRefresh={update}
                    ListFooterComponent={<FlatListFooterStub height={17} />}
                />
            )
        }
        let noContentView
        if (!contentView) {
            noContentView = (
                <ScrollView
                    style={layouts.containers.allAvailableSpace}
                    refreshControl={
                        <RefreshControl
                            refreshing={areProductsFetching}
                            onRefresh={update}
                        />
                    }
                >
                    <Text style={layouts.basicText.messageText}>
                        {localizedString('purchaseList.downloaded')}
                    </Text>
                </ScrollView>
            )
        }
        return (
            <View style={layouts.containers.allAvailableSpace}>
                {contentView || noContentView}
            </View>
        )
    }
}

const mapStateToProps = (state: RootState): StateMapProps => ({
    products: getProducts(state),
    areProductsFetching: getAreProductsFetching(state),
    areProductsFetched: getAreProductsFetched(state),
})

const mapDispatchToProps = (dispatch): DispatchMapProps => ({
    buy: productId => dispatch(makePurchaseAsync(productId)),
    update: () => dispatch(updateProductsAsync()),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PurchaseList)
